<div class="products-dropdown">
    <a href="#0" class="cd-close">Close</a>
    
    <nav class="products-menu">
        
<ul class="products-list">
    <li><a href="page-single-product.php">Fibras recicladas</a>
    <ul>
    <li><a href="page-single-product.php">Estucados</a>
    <ul>
    <li><a href="page-single-product.php">Reverso</a></li>
    <li><a href="page-single-product.php">Reverso Madera</a></li>
    <li><a href="page-single-product.php">Reverso Blanco</a></li>
    <li><a href="page-single-product.php">Reverso Kraft</a></li>
    <li><a href="page-single-product.php">Reverso Negro</a></li>
    <li><a href="page-single-product.php">Reverso Estucado 2 Caras</a></li>
    </ul>
    </li>
    <li><a href="page-single-product.php">No estucados</a>
    <ul>
    <li><a href="#">Carton Arena</a></li>
    <li><a href="#">Carton Arena Satinado</a></li>
    <li><a href="#">Carton Bigris</a></li>
    <li><a href="#">Carton Skin Pack 1 Cara</a></li>
    <li><a href="#">Carton Skin Pack 2 Caras</a></li>
    <li><a href="#">Carton Teja</a></li>
    <li><a href="#">Carton Azulete</a></li>
    </ul>
    </li>
    </ul>
    </li>
    
    
    <li><a href="page-single-product.php">Fibras vírgenes</a>
    <ul>
    <li><a href="#">Estucados</a>
    <ul>
    <li><a href="#">Folding Reverso Blanco</a></li>
    <li><a href="#">Folding Reverso Blanco Supreme FB11</a></li>
    <li><a href="#">Folding Reverso Madera</a></li>
    <li><a href="#">Folding Reverso Kraft</a></li>
    <li><a href="#">Kraft Estucado 1 Cara</a></li>
    <li><a href="#">Cartulina Grafica 1 Cara</a></li>
    <li><a href="#">Cartulina Grafica 2 Caras</a></li>
    </ul>
    </li>
    <li><a href="#">No estucados</a>
    <ul>
    <li><a href="#">Kraft Liner</a></li>
    </ul>
    </li>
    </ul>
    </li>
    
    
    <li><a href="page-single-product.php">Cartón grueso</a>
    <ul>
    <li><a href="#">Contracolados</a>
    <ul>
    <li><a href="#">Contracolado Gris - Gris</a></li>
    <li><a href="#">Contracolado Blanco – Gris</a></li>
    <li><a href="#">Contracolado Blanco – Blanco</a></li>
    <li><a href="#">Contracolado Negro – Gris</a></li>
    <li><a href="#">Contracolado Negro - Negro Supreme</a></li>
    <li><a href="#">Contracolado Marrón - Marrón 100% reciclable</a></li>
    <li><a href="#">Contracolado Estucado – Estucado</a></li>
    <li><a href="#">Pankaster Crema</a></li>
    <li><a href="#">Cartón absorbente</a>
    <ul>
    <li><a href="#">Cartón absorbente prensado</a></li>
    <li><a href="#">Cartón absorbente no prensado</a></li>
    </ul>
    </li>
    <li><a href="#">Compactos</a>
    <ul>
    <li><a href="#">Compacto Gris – Gris</a></li>
    <li><a href="#">Compacto Blanco – Gris</a></li>
    <li><a href="#">Compacto Blanco – Gris</a></li>
    <li><a href="#">Compacto Negro - Negro</a></li>
    </ul>
    </li>
    </ul>
    </li>
    </ul>
    </li>
    
    
    <li><a href="page-single-product.php">Cartón ondulado</a>
    <ul>
    <li><a href="#">Minimicro (Canal F)</a>
    <ul>
    <li><a href="#">Canal Vista - Formato (2 papeles)</a>
    <ul>
    <li><a href="#">Formato Minimicro Cuero</a></li>
    <li><a href="#">Formato Minimicro Blanco</a></li>
    <li><a href="#">Formato Minimicro Kraft</a></li>
    <li><a href="#">Formato Minimicro Kraft Blanco</a></li>
    <li><a href="#">Formato Minimicro Estucado</a></li>
    <li><a href="#">Formato Minimicro Blanco Onda Blanca</a></li>
    <li><a href="#">Formato Minimicro Negro Onda Negra</a></li>
    </ul>
    </li>
    <li><a href="#">Canal Cerrada - Plancha (3 papeles)</a>
    <ul>
    <li><a href="#">Plancha Minimicro Cuero – Cuero</a></li>
    <li><a href="#">Plancha Minimicro Blanco – Cuero</a></li>
    <li><a href="#">Plancha Minimicro Blanco – Blanco</a></li>
    <li><a href="#">Plancha Minimicro Kraft – Kraft</a></li>
    <li><a href="#">Plancha Minimicro Kraft Blanco – Cuero</a></li>
    <li><a href="#">Plancha Minimicro Kraft Blanco – Blanco</a></li>
    <li><a href="#">Plancha Minimicro Kraft Blanco - Kraft Blanco</a></li>
    <li><a href="#">Plancha Minimicro Estucado – Cuero</a></li>
    <li><a href="#">Plancha Minimicro Estucado – Blanco</a></li>
    <li><a href="#">Plancha Minimicro Estucado – Estucado</a></li>
    <li><a href="#">Plancha Minimicro Blanco - Blanco Onda Blanca</a></li>
    <li><a href="#">Plancha Minimicro Negro - Negro Onda Negra</a></li>
    </ul>
    </li>
    <li><a href="#">Microcanal (Canal E)</a>
    <ul>
    <li><a href="#">Canal Vista - Formato (2 papeles)</a>
    <ul>
    <li><a href="#">Formato Microcanal Cuero</a></li>
    <li><a href="#">Formato Microcanal Blanco</a></li>
    <li><a href="#">Formato Microcanal Kraft</a></li>
    <li><a href="#">Formato Microcanal Kraft Blanco</a></li>
    <li><a href="#">Formato Microcanal Estucado</a></li>
    <li><a href="#">Formato Microcanal Blanco Onda Blanca</a></li>
    <li><a href="#">Formato Microcanal Negro Onda Negra</a></li>
    </ul>
    </li>
    <li><a href="#">Canal Cerrada - Plancha (3 papeles)</a>
    <ul>
    <li><a href="#">Plancha Microcanal Cuero – Cuero</a></li>
    <li><a href="#">Plancha Microcanal Blanco – Cuero</a></li>
    <li><a href="#">Plancha Microcanal Blanco – Blanco</a></li>
    <li><a href="#">Plancha Microcanal Kraft - Kraft</a></li>
    <li><a href="#">Plancha Microcanal Kraft Blanco – Cuero</a></li>
    <li><a href="#">Plancha Microcanal Kraft Blanco – Blanco</a></li>
    <li><a href="#">Plancha Microcanal Kraft Blanco - Kraft Blanco</a></li>
    <li><a href="#">Plancha Microcanal Estucado – Cuero</a></li>
    <li><a href="#">Plancha Microcanal Estucado – Blanco</a></li>
    <li><a href="#">Plancha Microcanal Estucado – Estucado</a></li>
    <li><a href="#">Plancha Microcanal Kraft Estucado - Kraft Estucado</a></li>
    <li><a href="#">Plancha Microcanal Blanco - Blanco Onda Blanca</a></li>
    <li><a href="#">Plancha Microcanal Negro - Negro Onda Negra</a></li>
    </ul>
    </li>
    <li><a href="#">Canal 3 (Canal B)</a>
    <ul>
    <li><a href="#">Canal Vista - Formato (2 papeles)</a>
    <ul>
    <li><a href="#">Formato Canal 3 Cuero</a></li>
    <li><a href="#">Formato Canal 3 Blanco</a></li>
    <li><a href="#">Formato Canal 3 Kraft</a></li>
    <li><a href="#">Formato Canal 3 Kraft Blanco</a></li>
    <li><a href="#">Formato Canal 3 Estucado</a></li>
    <li><a href="#">Formato Canal 3 Blanco Onda Blanca</a></li>
    <li><a href="#">Formato Canal 3 Negro Onda Negra</a></li>
    </ul>
    </li>
    <li><a href="#">Canal Cerrada - Plancha (3 papeles)</a>
    <ul>
    <li><a href="#">Plancha Canal 3 Cuero – Cuero</a></li>
    <li><a href="#">Plancha Canal 3 Blanco – Cuero</a></li>
    <li><a href="#">Plancha Canal 3 Blanco – Blanco</a></li>
    <li><a href="#">Plancha Canal 3 Kraft – Kraft</a></li>
    <li><a href="#">Plancha Canal 3 Kraft Blanco – Cuero</a></li>
    <li><a href="#">Plancha Canal 3 Kraft Blanco – Blanco</a></li>
    <li><a href="#">Plancha Canal 3 Kraft Blanco - Kraft Blanco</a></li>
    <li><a href="#">Plancha Canal 3 Estucado – Cuero</a></li>
    <li><a href="#">Plancha Canal 3 Estucado – Blanco</a></li>
    <li><a href="#">Plancha Canal 3 Estucado – Estucado</a></li>
    <li><a href="#">Plancha Canal 3 Blanco - Blanco Onda Blanca</a></li>
    <li><a href="#">Plancha Canal 3 Negro - Negro Onda Negra</a></li>
    </ul>
    </li>
    <li><a href="#">Canal 5 (Canal C)</a>
    <ul>
    <li><a href="#">Canal Vista - Formato (2 papeles)</a>
    <ul>
    <li><a href="#">Formato Canal 5</a></li>
    <li><a href="#">Formato Canal 5 Blanco</a></li>
    <li><a href="#">Formato Canal 5 Kraft</a></li>
    <li><a href="#">Formato Canal 5 Kraft Blanco</a></li>
    <li><a href="#">Formato Canal 5 Estucado</a></li>
    </ul>
    </li>
    <li><a href="#">Canal Cerrada - Plancha (3 papeles)</a>
    <ul>
    <li><a href="#">Plancha Canal 5 Cuero – Cuero</a></li>
    <li><a href="#">Plancha Canal 5 Blanco – Cuero</a></li>
    <li><a href="#">Plancha Canal 5 Blanco – Blanco</a></li>
    <li><a href="#">Plancha Canal 5 Kraft – Kraft</a></li>
    <li><a href="#">Plancha Canal 5 Kraft Blanco – Cuero</a></li>
    <li><a href="#">Plancha Canal 5 Kraft Blanco – Blanco</a></li>
    <li><a href="#">Plancha Canal 5 Kraft Blanco - Kraft Blanco</a></li>
    <li><a href="#">Plancha Canal 5 Estucado – Cuero</a></li>
    <li><a href="#">Plancha Canal 5 Estucado – Blanco</a></li>
    <li><a href="#">Plancha Canal 5 Estucado - Estucado</a></li>
    </ul>
    </li>
    <li><a href="#">Microcanal + Minimicro (Canal EF)</a>
    <ul>
    <li><a href="#">Canal Vista - Doble Canal (4 papeles)</a>
    <ul>
    <li><a href="#">Microcanal + Minimicro Cuero</a></li>
    <li><a href="#">Microcanal + Minimicro Blanco</a></li>
    <li><a href="#">Microcanal + Minimicro Kraft</a></li>
    <li><a href="#">Microcanal + Minimicro Kraft Blanco</a></li>
    <li><a href="#">Microcanal + Minimicro Estucado</a></li>
    </ul>
    </li>
    <li><a href="#">Canal Cerrada - Plancha (5 papeles)</a>
    <ul>
    <li><a href="#">Microcanal + Minimicro Cuero – Cuero</a></li>
    <li><a href="#">Microcanal + Minimicro Blanco – Cuero</a></li>
    <li><a href="#">Microcanal + Minimicro Blanco – Blanco</a></li>
    <li><a href="#">Microcanal + Minimicro Kraft – Kraft</a></li>
    <li><a href="#">Microcanal + Minimicro Kraft Blanco – Cuero</a></li>
    <li><a href="#">Microcanal + Minimicro Kraft Blanco – Blanco</a></li>
    <li><a href="#">Microcanal + Minimicro Kraft Blanco - Kraft Blanco</a></li>
    <li><a href="#">Microcanal + Minimicro Estucado – Cuero</a></li>
    <li><a href="#">Microcanal + Minimicro Estucado – Blanco</a></li>
    <li><a href="#">Microcanal + Minimicro Estucado - Estucado</a></li>
    </ul>
    </li>
    <li><a href="#">Microcanal + Microcanal (Canal EE)</a>
    <ul>
    <li><a href="#">Canal Vista - Doble Canal (4 papeles)</a>
    <ul>
    <li><a href="#">Microcanal + Microcanal Cuero</a></li>
    <li><a href="#">Microcanal + Microcanal Blanco</a></li>
    <li><a href="#">Microcanal + Microcanal Kraft</a></li>
    <li><a href="#">Microcanal + Microcanal Kraft Blanco</a></li>
    <li><a href="#">Microcanal + Microcanal Estucado</a></li>
    </ul>
    </li>
    <li><a href="#">Canal Cerrada - Plancha (5 papeles)</a>
    <ul>
    <li><a href="#">Microcanal + Microcanal Cuero – Cuero</a></li>
    <li><a href="#">Microcanal + Microcanal Blanco – Cuero</a></li>
    <li><a href="#">Microcanal + Microcanal Blanco – Blanco</a></li>
    <li><a href="#">Microcanal + Microcanal Kraft – Kraft</a></li>
    <li><a href="#">Microcanal + Microcanal Kraft Blanco – Cuero</a></li>
    <li><a href="#">Microcanal + Microcanal Kraft Blanco – Blanco</a></li>
    <li><a href="#">Microcanal + Microcanal Kraft Blanco - Kraft Blanco</a></li>
    <li><a href="#">Microcanal + Microcanal Estucado – Cuero</a></li>
    <li><a href="#">Microcanal + Microcanal Estucado – Blanco</a></li>
    <li><a href="#">Microcanal + Microcanal Estucado – Estucado</a></li>
    <li><a href="#">Microcanal + Microcanal Kraft Estucado - Kraft Estucado</a></li>
    </ul>
    </li>
    <li><a href="#">Microcanal + Canal 3 (Canal EB)</a>
    <ul>
    <li><a href="#">Canal Cerrada - Plancha (5 papeles)</a>
    <ul>
    <li><a href="#">Microcanal + Canal 3 Cuero – Cuero</a></li>
    <li><a href="#">Microcanal + Canal 3 Blanco – Cuero</a></li>
    <li><a href="#">Microcanal + Canal 3 Blanco – Blanco</a></li>
    <li><a href="#">Microcanal + Canal 3 Kraft – Kraft</a></li>
    <li><a href="#">Microcanal + Canal 3 Kraft Blanco – Cuero</a></li>
    <li><a href="#">Microcanal + Canal 3 Kraft Blanco – Blanco</a></li>
    <li><a href="#">Microcanal + Canal 3 Kraft Blanco - Kraft Blanco</a></li>
    <li><a href="#">Microcanal + Canal 3 Estucado – Cuero</a></li>
    <li><a href="#">Microcanal + Canal 3 Estucado – Blanco</a></li>
    <li><a href="#">Microcanal + Canal 3 Estucado – Estucado</a></li>
    <li><a href="#">Microcanal + Canal 3 Kraft Estucado - Kraft Estucado</a></li>
    </ul>
    </li>
    <li><a href="#">Microcanal + Canal 5 (Canal EC)</a>
    <ul>
    <li><a href="#">Canal Cerrada - Plancha (5 papeles)</a>
    <ul>
    <li><a href="#">Microcanal + Canal 5 Cuero – Cuero</a></li>
    <li><a href="#">Microcanal + Canal 5 Blanco – Cuero</a></li>
    <li><a href="#">Microcanal + Canal 5 Blanco – Blanco</a></li>
    <li><a href="#">Microcanal + Canal 5 Kraft – Kraft</a></li>
    <li><a href="#">Microcanal + Canal 5 Kraft Blanco – Cuero</a></li>
    <li><a href="#">Microcanal + Canal 5 Kraft Blanco – Blanco</a></li>
    <li><a href="#">Microcanal + Canal 5 Kraft Blanco - Kraft Blanco</a></li>
    <li><a href="#">Microcanal + Canal 5 Estucado – Cuero</a></li>
    <li><a href="#">Microcanal + Canal 5 Estucado – Blanco</a></li>
    <li><a href="#">Microcanal + Canal 5 Estucado - Estucado</a></li>
    </ul>
    </li>
    <li><a href="#">Canal 3 + Canal 5 (CanalBC)</a>
    <ul>
    <li><a href="#">Canal Cerrada - Plancha (5 papeles)</a>
    <ul>
    <li><a href="#">Doble Doble Cuero – Cuero</a></li>
    <li><a href="#">Doble Doble Blanco – Cuero</a></li>
    <li><a href="#">Doble Doble Blanco – Blanco</a></li>
    <li><a href="#">Doble Doble Kraft – Kraft</a></li>
    <li><a href="#">Doble Doble Kraft Blanco – Cuero</a></li>
    <li><a href="#">Doble Doble Kraft Blanco – Blanco</a></li>
    <li><a href="#">Doble Doble Kraft Blanco - Kraft Blanco</a></li>
    <li><a href="#">Doble Doble Estucado – Cuero</a></li>
    <li><a href="#">Doble Doble Estucado – Blanco</a></li>
    <li><a href="#">Doble Doble Estucado - Estucado</a></li>
    </ul>
    </li>
    </ul>
    </li>
    </ul>
    </li>
    </ul>
    </li>
    </ul>
    </li>
    </ul>
    </li>
    </ul>
    </li>
    </ul>
    </li>
    </ul>
    </li>
    </ul>
    </li>
    </ul>
    </li>
    
    
    <li><a href="page-single-product.php">Cartulinas recicladas de colores</a>
    <ul>
    <li><a href="#">Cartulina de color Albatros</a>
    <ul>
    <li><a href="#">325 gr/m2</a></li>
    <li><a href="#">450 gr/m2</a></li>
    <li><a href="#">650 gr/m2</a></li>
    </ul>
    </li>
    <li><a href="#">Cartulina de color Antracita</a>
    <ul>
    <li><a href="#">325 gr/m2</a></li>
    <li><a href="#">450 gr/m2</a></li>
    <li><a href="#">650 gr/m2</a></li>
    </ul>
    </li>
    <li><a href="#">Cartulina de color Turmalina</a>
    <ul>
    <li><a href="#">325 gr/m2</a></li>
    <li><a href="#">450 gr/m2</a></li>
    <li><a href="#">650 gr/m2</a></li>
    </ul>
    </li>
    <li><a href="#">Cartulina de color Topacio</a>
    <ul>
    <li><a href="#">325 gr/m2</a></li>
    <li><a href="#">450 gr/m2</a></li>
    <li><a href="#">650 gr/m2</a></li>
    </ul>
    </li>
    <li><a href="#">Cartulina de color Azul noche</a>
    <ul>
    <li><a href="#">325 gr/m2</a></li>
    <li><a href="#">450 gr/m2</a></li>
    <li><a href="#">650 gr/m2</a></li>
    </ul>
    </li>
    <li><a href="#">Cartulina de color Gres</a>
    <ul>
    <li><a href="#">325 gr/m2</a></li>
    <li><a href="#">450 gr/m2</a></li>
    <li><a href="#">650 gr/m2</a></li>
    </ul>
    </li>
    <li><a href="#">Cartulina de color Esmeralda</a>
    <ul>
    <li><a href="#">325 gr/m2</a></li>
    <li><a href="#">450 gr/m2</a></li>
    <li><a href="#">650 gr/m2</a></li>
    </ul>
    </li>
    <li><a href="#">Cartulina de color Amatista</a>
    <ul>
    <li><a href="#">325 gr/m2</a></li>
    <li><a href="#">450 gr/m2</a></li>
    <li><a href="#">650 gr/m2</a></li>
    </ul>
    </li>
    <li><a href="#">Cartulina de color Sable</a>
    <ul>
    <li><a href="#">325 gr/m2</a></li>
    <li><a href="#">450 gr/m2</a></li>
    <li><a href="#">650 gr/m2</a></li>
    </ul>
    </li>
    <li><a href="#">Cartulina de color Oliva</a>
    <ul>
    <li><a href="#">325 gr/m2</a></li>
    <li><a href="#">450 gr/m2</a></li>
    <li><a href="#">650 gr/m2</a></li>
    </ul>
    </li>
    <li><a href="#">Cartulina de color Zafiro</a>
    <ul>
    <li><a href="#">325 gr/m2</a></li>
    <li><a href="#">450 gr/m2</a></li>
    <li><a href="#">650 gr/m2</a></li>
    </ul>
    </li>
    <li><a href="#">Cartulina de color Rubí</a>
    <ul>
    <li><a href="#">325 gr/m2</a></li>
    <li><a href="#">450 gr/m2</a></li>
    <li><a href="#">650 gr/m2</a></li>
    </ul>
    </li>
    <li><a href="#">Cartulina de color Chocolate</a>
    <ul>
    <li><a href="#">325 gr/m2</a></li>
    <li><a href="#">450 gr/m2</a></li>
    <li><a href="#">650 gr/m2</a></li>
    </ul>
    </li>
    <li><a href="#">Cartulina de color Naranja</a>
    <ul>
    <li><a href="#">325 gr/m2</a></li>
    <li><a href="#">450 gr/m2</a></li>
    <li><a href="#">650 gr/m2</a></li>
    </ul>
    </li>
    <li><a href="#">Cartulina de color Piedra</a>
    <ul>
    <li><a href="#">325 gr/m2</a></li>
    <li><a href="#">450 gr/m2</a></li>
    <li><a href="#">650 gr/m2</a></li>
    </ul>
    </li>
    <li><a href="#">Cartulina de color Pizarra</a>
    <ul>
    <li><a href="#">325 gr/m2</a></li>
    <li><a href="#">450 gr/m2</a></li>
    <li><a href="#">650 gr/m2</a></li>
    </ul>
    </li>
    </ul>
    </li>
    
    
    <li><a href="page-single-product.php">Cartón pluma</a>
    <ul>
    <li><a href="#">3mm – Sin adhesivo</a>
    <ul>
    <li><a href="#">Carton Pluma Blanco</a></li>
    <li><a href="#">Carton Pluma Blanco Supreme</a></li>
    <li><a href="#">Carton Pluma Blanco Libre de Acidos</a></li>
    <li><a href="#">Carton Pluma Blanco – Kraft</a></li>
    <li><a href="#">Carton Pluma Kraft – Kraft</a></li>
    <li><a href="#">Carton Pluma Negro</a></li>
    <li><a href="#">Carton Pluma Alu-Foam</a></li>
    </ul>
    </li>
    <li><a href="#">3mm – Adhesivo 1 cara</a>
    <ul>
    <li><a href="#">Carton Pluma Blanco</a></li>
    <li><a href="#">Carton Pluma Blanco Supreme</a></li>
    <li><a href="#">Carton Pluma Blanco Libre de Acidos</a></li>
    <li><a href="#">Carton Pluma Blanco – Kraft</a></li>
    <li><a href="#">Carton Pluma Kraft – Kraft</a></li>
    <li><a href="#">Carton Pluma Negro</a></li>
    <li><a href="#">Carton Pluma Alu-Foam</a></li>
    </ul>
    </li>
    <li><a href="#">5mm – Sin adhesivo</a>
    <ul>
    <li><a href="#">Carton Pluma Blanco</a></li>
    <li><a href="#">Carton Pluma Blanco Supreme</a></li>
    <li><a href="#">Carton Pluma Blanco Libre de Acidos</a></li>
    <li><a href="#">Carton Pluma Blanco – Kraft</a></li>
    <li><a href="#">Carton Pluma Kraft – Kraft</a></li>
    <li><a href="#">Carton Pluma Negro</a></li>
    <li><a href="#">Carton Pluma Alu-Foam</a></li>
    </ul>
    </li>
    <li><a href="#">5mm – Adhesivo 1 cara</a>
    <ul>
    <li><a href="#">Carton Pluma Blanco</a></li>
    <li><a href="#">Carton Pluma Blanco Supreme</a></li>
    <li><a href="#">Carton Pluma Blanco Libre de Acidos</a></li>
    <li><a href="#">Carton Pluma Blanco – Kraft</a></li>
    <li><a href="#">Carton Pluma Kraft – Kraft</a></li>
    <li><a href="#">Carton Pluma Negro</a></li>
    <li><a href="#">Carton Pluma Alu-Foam</a></li>
    </ul>
    </li>
    <li><a href="#">10mm – Sin adhesivo</a>
    <ul>
    <li><a href="#">Carton Pluma Blanco</a></li>
    <li><a href="#">Carton Pluma Blanco Supreme</a></li>
    <li><a href="#">Carton Pluma Blanco Libre de Acidos</a></li>
    <li><a href="#">Carton Pluma Blanco – Kraft</a></li>
    <li><a href="#">Carton Pluma Kraft – Kraft</a></li>
    <li><a href="#">Carton Pluma Negro</a></li>
    <li><a href="#">Carton Pluma Alu-Foam</a></li>
    </ul>
    </li>
    <li><a href="#">10mm – Adhesivo 1 cara</a>
    <ul>
    <li><a href="#">Carton Pluma Blanco</a></li>
    <li><a href="#">Carton Pluma Blanco Supreme</a></li>
    <li><a href="#">Carton Pluma Blanco Libre de Acidos</a></li>
    <li><a href="#">Carton Pluma Blanco – Kraft</a></li>
    <li><a href="#">Carton Pluma Kraft – Kraft</a></li>
    <li><a href="#">Carton Pluma Negro</a></li>
    <li><a href="#">Carton Pluma Alu-Foam</a></li>
    </ul>
    </li>
    </ul>
    </li>
    
    
    <li><a href="page-single-product.php">Papeles alimentación</a>
    <ul>
    <li><a href="#">Papel parafinado 2 caras 30gr/m2</a></li>
    <li><a href="#">Papel parafinado 2 caras 40gr/m2</a></li>
    <li><a href="#">Papel parafinado 2 caras 60gr/m2</a></li>
    <li><a href="#">Papel parafinado 2 caras 70gr/m2</a></li>
    <li><a href="#">Papel parafinado 2 caras 80gr/m2</a></li>
    <li><a href="#">Papel parafinado 1 cara 60gr/m2</a></li>
    <li><a href="#">Papel sulfurizado 41gr/m2</a></li>
    <li><a href="#">Papel siliconado 40gr/m2</a></li>
    <li><a href="#">Papel Manila blanco 24gr/m2</a></li>
    <li><a href="#">Papel Manila moreno 24gr/m2</a></li>
    <li><a href="#">Papel Chocolate 5 papeles</a></li>
    <li><a href="#">Almohadilla blanco5 papeles</a></li>
    <li><a href="#">Almohadilla Chocolate 5 papeles</a></li>
    <li><a href="#">Almohadilla Negro 5 papeles</a></li>
    </ul>
    </li>
    <li><a href="#">Papeles regalo, decoración y diseño</a>
    <ul>
    <li><a href="#">Papel seda Blanco 17gr/m2</a></li>
    <li><a href="#">Papel seda Negro 17gr/m2</a></li>
    <li><a href="#">Papel seda Rojo 17gr/m2</a></li>
    <li><a href="#">Papel seda Azul oscuro 17gr/m2</a></li>
    <li><a href="#">Papel seda Celeste 17gr/m2</a></li>
    <li><a href="#">Papel seda Marrón 17gr/m2</a></li>
    <li><a href="#">Papel seda Verde oscuro 17gr/m2</a></li>
    <li><a href="#">Papel seda Verde claro 17gr/m2</a></li>
    <li><a href="#">Papel seda Amarillo 17gr/m2</a></li>
    <li><a href="#">Papel seda Naranja 17gr/m2</a></li>
    <li><a href="#">Papel seda Fucsia 17gr/m2</a></li>
    <li><a href="#">Papel seda Rosa claro 17gr/m2</a></li>
    <li><a href="#">Papel seda Oro 17gr/m2</a></li>
    <li><a href="#">Papel seda Plata 17gr/m2</a></li>
    <li><a href="#">Papel posavasos Blanco 200gr/m2</a></li>
    <li><a href="#">Papel posavasos Blanco 230gr/m2</a></li>
    <li><a href="#">Papel posavasos Blanco 250gr/m2</a></li>
    <li><a href="#">Papel posavasos Blanco 350gr/m2</a></li>
    </ul>
    </li>
    <li><a href="#">Material de embalaje</a>
    <ul>
    <li><a href="#">Bobinas de cartón ondulado</a>
    <ul>
    <li><a href="#">Formato Microcanal</a></li>
    <li><a href="#">Formato Canal 3</a></li>
    </ul>
    </li>
    <li><a href="#">Tubos de cartón</a>
    <ul>
    <li><a href="#">Acabado Kraft</a></li>
    <li><a href="#">Acabado Blanco Charol</a></li>
    </ul>
    </li>
    <li><a href="#">Tapas de plástico</a>
    <ul>
    <li><a href="#">Blancas</a></li>
    <li><a href="#">Negras</a></li>
    </ul>
    </li>
    </ul>
    </li>
    
    
    <li><a href="page-single-product.php">Fundas elásticas de cartón</a></li>
    <li><a href="page-single-product.php">Ondas de cartón</a></li>
</ul>
        
    </nav>
    
</div>