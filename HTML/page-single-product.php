<?php
$bodyClass = 'single-product';
include 'header.php';
?>


<section class="billboard"></section>


<main>
    
    <section class="single-product-section container">
        
        <div class="row">

            <div class="col image">
                <ul id="image-gallery" class="gallery">
                    <li data-thumb="assets/images/th_shop-1.jpg"><img width="900" height="600" src="assets/images/products/ref_40_corba_01.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></li>
                    <li data-thumb="assets/images/th_shop-1-2.jpg"><img width="900" height="600" src="assets/images/products/ref_40_corba_02.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></li>
                    <li data-thumb="assets/images/th_shop-1-3.jpg"><img width="900" height="600" src="assets/images/products/ref_40_verso.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></li>
                </ul>
            </div>

            <div class="col content">
                <div class="product_header entry-header">
                    <div class="row">
                        <div class="col col-10">
                            <span class="col-title">Producto</span>
                            <nav>
                                <ul id="breadcrumbs" class="breadcrumbs">
                                    <li><a href="page-catagories.php">Fibras recicladas</a></li> &sol; 
                                    <li><a href="page-products.php">No estucados</a></li> &sol; 
                                    <li>Cartón arena satinado</li>
                                </ul>
                            </nav>

                            <h1 class="product_title entry-title">Cartón arena satinado</h1>
                        </div>
                        <div class="col col-2">
                            <span class="col-title">Referéncia</span>
                            <p>163</p>
                        </div>
                    </div>
                </div>
                
                <div class="product_content entry-content">
                    <div class="row">
                        <div class="col">
                            <span class="col-title">Descripción</span>
                            <p>El cartón de la calidad Arena, Bigris y Skin Pack es producido 100 % de fibras recicladas postconsumo.</p>
                            <p>Cartoncillo reciclado, reciclable, biodegradable y compostable.</p>
                            <p class="suitable"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-checkmark"></use></svg>Apto para el contacto con alimentos.</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <span class="col-title">Aplicaciones</span>
                            <p>Envases, estuches, packaging para los sectores alimentario, seguridad y protección, automóvil, textil, calzado, jardinería, construcción, ingeniería, tecnología...</p>
                            <p>Sus aplicaciones más conocidas son: separador de palets, fondo de bolsas, packaging creativo, decoración y diseño, scrapbooking, PLV y fabricación de tubos de cartón y cantoneras.</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <span class="col-title">Calibres</span>
                        </div>
                        <div class="col">
                            <span class="col-title">Medidas</span>
                            <p>A su medida deseada</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <span class="col-title">Gramajes</span>
                            <p>Desde 300 a 650 gr/m²</p>
                        </div>
                        <div class="col">
                            <span class="col-title">Observaciones</span>
                            <p>Otras medidas, gramajes o cantidades, consultar</p>
                        </div>
                    </div>
                </div><!-- /product_content -->
                
                <div class="product_downloads">
                    <div class="row">
                        <div class="col">
                            <a href="#" class="button" title="Descargar ficha técnica">Ficha técnica<svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-file-download-alt"></use></svg></a>
                            <a href="#" class="button" title="Descargar certificado">Certificado<svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-certificate"></use></svg></a>
                        </div>
                    </div>
                </div>
                
            </div>

        </div>
        
    </section>

    
    <?php include("content-aside.php"); ?>
    
</main>


<?php include("footer.php"); ?>