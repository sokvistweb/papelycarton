<?php
$bodyClass = 'help';
include 'header.php';
?>


<section class="billboard">
    
</section>


<main>
    
    <section class="help-section container">
        
        <div class="row">
            <div class="col">
                <h1>¿Te ayudamos?</h1>
            </div>
        </div>
        
        <div class="row help-content">
            <div class="col col-4">
                <p>Hace 25 años que trabajamos con el cartón. Tenemos experiencia y conocimiento basándonos en casos reales de miles de clientes para asesorarte sobre qué tipo de cartón es la mejor solución para lo que tienes en mente.</p>
                <p>Déjanos tus datos y uno de nuestros consultores te ofrecerá asesoramiento personalizado.</p>
            </div>
            
            <div class="col col-8">
                <div role="form" class="wpcf7" id="wpcf7-f203-p34-o1" lang="ca" dir="ltr">
                    <div class="screen-reader-response" role="alert" aria-live="polite"></div>
                    <form action="" method="post" class="wpcf7-form init" novalidate="novalidate">
                    <div style="display: none;">
                    <input type="hidden" name="_wpcf7" value="203">
                    <input type="hidden" name="_wpcf7_version" value="5.2">
                    <input type="hidden" name="_wpcf7_locale" value="ca">
                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f203-p34-o1">
                    <input type="hidden" name="_wpcf7_container_post" value="34">
                    <input type="hidden" name="_wpcf7_posted_data_hash" value="">
                    </div>
                    <p><label> Nombre (obligatorio)<br>
                        <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span> </label></p>
                    <p><label> Teléfono (obligatorio)<br>
                        <span class="wpcf7-form-control-wrap your-email"><input type="tel" name="your-tel" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span> </label></p>
                    <p><label> Email (obligatorio)<br>
                        <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span> </label></p>
                    
                    <p>
                        <span class="wpcf7-form-control-wrap radio-client">
                            <span class="wpcf7-form-control wpcf7-radio">
                                <span class="wpcf7-list-item first"><label><input type="radio" name="radio-horari" value="not-client" class="">
                                    <span class="wpcf7-list-item-label">No soy cliente</span></label>
                                </span>
                                <span class="wpcf7-list-item last"><label><input type="radio" name="radio-horari" value="client">
                                    <span class="wpcf7-list-item-label">Soy cliente</span></label>
                                </span>
                            </span>
                        </span>
                    </p>
                    
                    <div class="radio-fields">
                        <div class="box-fields not-client">                            
                            <p><strong>¿Qué tipo de ayuda necesitas?</strong><br>
                                <span class="wpcf7-form-control-wrap radio-help">
                                    <span class="wpcf7-form-control wpcf7-radio">
                                        <span class="wpcf7-list-item first"><label><input type="radio" name="radio-horari" value="9:30h"><span class="wpcf7-list-item-label">Presupuesto</span></label>
                                        </span>
                                        <span class="wpcf7-list-item last"><label><input type="radio" name="radio-horari" value="13:00h"><span class="wpcf7-list-item-label">Resolución de dudas sobre producto</span></label>
                                        </span>
                                    </span>
                                </span>
                            </p>
                        </div>
                        <div class="box-fields client">
                            <p><strong>Nombre de la empresa</strong><br>
                                <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
                            </p>
                        </div>
                    </div>
                    
                    <p class="p-accept"><span class="wpcf7-form-control-wrap accept"><span class="wpcf7-form-control wpcf7-acceptance"><span class="wpcf7-list-item"><input type="checkbox" name="accept" value="1" aria-invalid="false"></span></span></span> He leído y acepto su <a href="/politica-de-privacitat">política de privacidad</a>.</p>
                    <p><input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" disabled=""><span class="ajax-loader"></span></p>
                    <div class="wpcf7-response-output" role="alert" aria-hidden="true">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
    </section>
    
    <?php include("content-aside.php"); ?>
    
</main>


<?php include("footer.php"); ?>