<?php
$bodyClass = 'account';
include 'header.php';
?>


<section class="billboard">
    
</section>


<main>
    
    <section class="members-section container">
        
        <div class="row">
            <div class="col">
                <h1>Tu Cuenta</h1>
            </div>
        </div>
        
        <div class="row members-content">
            <div class="col col-4">
                
                <h2>Acceder</h2>
                
            </div>
            
            <div class="col col-8">

                <form class="woocommerce-form woocommerce-form-login login" method="post">
			
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="username">Nombre de usuario o correo electrónico&nbsp;<span class="required">*</span></label>
                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" autocomplete="username" value="">			</p>
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="password">Contraseña&nbsp;<span class="required">*</span></label>
                        <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" autocomplete="current-password">
                    </p>

                    <p class="form-row">
                        <label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
                            <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever"> <span>Recuérdame</span>
                        </label>
                        <input type="hidden" id="woocommerce-login-nonce" name="woocommerce-login-nonce" value="cc8dd7dc30"><input type="hidden" name="_wp_http_referer" value="/es/my-account/">				<button type="submit" class="woocommerce-button button woocommerce-form-login__submit" name="login" value="Acceder">Acceder</button>
                    </p>
                    <p class="woocommerce-LostPassword lost_password">
                        <a href="http://bellalola/es/my-account/lost-password/">¿Olvidaste la contraseña?</a>
                    </p>

                </form>
                
            </div>
        </div>
        
    </section>
    
    <?php include("content-aside.php"); ?>
    
</main>


<?php include("footer.php"); ?>