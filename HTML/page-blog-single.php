<?php
$bodyClass = 'single';
include 'header.php';
?>


<section class="billboard">
    
</section>


<main>
    
    <section class="blog-section container">
        
        <div class="row">
            <div class="col col-9">
                <h1>Dificultades principales que se pueden encontrar al trabajar un cartón</h1>
            </div>
            <div class="col col-3"></div>
        </div>
        
        <div class="row">
            <div class="col col-9">
                
                <div class="row entry-content single-content">
                    <div class="col">
                        <img width="900" height="600" src="assets/images/blog/problemas_ii_2.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="">
                        <img width="900" height="600" src="assets/images/blog/problemas_ii_1_0.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="">
                    </div>
                    
                    <div class="col">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis at consectetur lorem donec massa. Feugiat in ante metus dictum at tempor.</p>
                        
                        <h2>Aliquet nibh praesent tristique magna</h2>

                        <p>Sapien et ligula ullamcorper malesuada. Arcu dui vivamus arcu felis. Massa sed elementum tempus egestas. Dolor sit amet consectetur adipiscing elit. </p>
                        
                        <h3>Dignissim suspendisse in est ante</h3>
                        <p>Arcu ac tortor dignissim convallis. Scelerisque varius morbi enim nunc faucibus a pellentesque sit amet. Pharetra sit amet aliquam id diam maecenas ultricies mi eget.</p>
                        <p>Duis at consectetur lorem donec massa. Feugiat in ante metus dictum at tempor. Feugiat in ante metus dictum at tempor.</p>
                    </div>
                </div>
                
            </div>
            
            <div class="col col-3">
                <div id="categories-3" class="widget widget_categories">
                    <h3>Categorias</h3>
                    <ul>
                        <li class="cat-item cat-item-59"><a href="page-blog.php">Técnica</a></li>
                        <li class="cat-item cat-item-61"><a href="page-blog.php">Tendencias</a></li>
                        <li class="cat-item cat-item-80"><a href="page-blog.php">Medio ambiente</a></li>
                        <li class="cat-item cat-item-86"><a href="page-blog.php">Novedades</a></li>
                        <li class="cat-item cat-item-53"><a href="page-blog.php">Ideas</a></li>
                    </ul>
                </div>
                <div id="recent-posts-3" class="widget widget_recent_entries">
                    <h3>Entradas recientes</h3>
                    <ul>
				        <li><a href="page-blog.php">¿Como es el proceso de fabricación del cartón?</a></li>
						<li><a href="page-blog.php">Papeles especiales</a></li>
                        <li><a href="page-blog.php">Cartón reciclado: una tendencia por la protección del medio ambiente</a></li>
                        <li><a href="page-blog.php">Figuras de papel para montárselo bien</a></li>
                        <li><a href="page-blog.php">Dificultades principales que se pueden encontrar al trabajar un cartón</a></li>
					</ul>
                </div>
            </div>
        </div>
        
    </section>
    
    <?php include("content-aside.php"); ?>
    
</main>


<?php include("footer.php"); ?>