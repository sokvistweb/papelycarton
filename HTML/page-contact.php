<?php
$bodyClass = 'contact';
include 'header.php';
?>


<section class="billboard">
    
</section>


<main>
    
    <section class="categories-section container">
        
        <div class="row">
            <div class="col">
                <h1>Contacto</h1>
            </div>
        </div>
        
        <div class="row help-content contact-content">
            <div class="col col-4">
                <address>C/ Garnatxa,12<br>
                    Parc Empresarial de Cervelló<br>
                    08758 Cervelló (Barcelona)</address>

                <p>Teléfono: <a href="tel:0034936730525">+34 93 673 05 25</a><br>
                    Fax: <a href="tel:0034936730525">+34 93 673 05 26</a><br>
                <a href="mailto:vegio@vegio.es">vegio@vegio.es</a>
                </p>
            </div>
            
            <div class="col col-8">
                <div role="form" class="wpcf7" id="wpcf7-f203-p34-o1" lang="ca" dir="ltr">
                    <div class="screen-reader-response" role="alert" aria-live="polite"></div>
                    <form action="/es/contacte/#wpcf7-f203-p34-o1" method="post" class="wpcf7-form init" novalidate="novalidate">
                    <div style="display: none;">
                    <input type="hidden" name="_wpcf7" value="203">
                    <input type="hidden" name="_wpcf7_version" value="5.2">
                    <input type="hidden" name="_wpcf7_locale" value="ca">
                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f203-p34-o1">
                    <input type="hidden" name="_wpcf7_container_post" value="34">
                    <input type="hidden" name="_wpcf7_posted_data_hash" value="">
                    </div>
                    <p><label> Nombre (obligatorio)<br>
                        <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span> </label></p>
                    <p><label> Email (obligatorio)<br>
                        <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span> </label></p>
                    <p><label> Mensaje<br>
                        <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span> </label></p>
                    <p class="p-accept"><span class="wpcf7-form-control-wrap accept"><span class="wpcf7-form-control wpcf7-acceptance"><span class="wpcf7-list-item"><input type="checkbox" name="accept" value="1" aria-invalid="false"></span></span></span> He leído y acepto su <a href="https://edetaria.com/es/politica-de-privadesa/">política de privacidad</a>.</p>
                    <p><input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" disabled=""><span class="ajax-loader"></span></p>
                    <div class="wpcf7-response-output" role="alert" aria-hidden="true"></div></form>
                </div>
            </div>
        </div>
        
    </section>
    
    <?php include("content-aside.php"); ?>
    
</main>


<?php include("footer.php"); ?>