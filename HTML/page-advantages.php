<?php
$bodyClass = 'advantages';
include 'header.php';
?>


<section class="billboard">
    
</section>


<main>
    
    <section class="advantages-section container">
        
        <div class="row">
            <div class="col">
                <h1>Tus Ventajas</h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col">
                <nav>
                    <ul class="advantages-list">
                        <li><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-checkmark"></use></svg>Corte a medida.</li>
                        <li><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-checkmark"></use></svg>Cantidad según tus necesidades.</li>
                        <li><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-checkmark"></use></svg>Compra sólo lo que necesites, el stock te lo guardamos nosotros.</li>
                        <li><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-checkmark"></use></svg>Preparamos pedidos a partir de 25 kg.</li>
                        <li><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-checkmark"></use></svg>Solicítanos muestras para presentar tus maquetas o proyectos.</li>
                        <li><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-checkmark"></use></svg>Regístrate y accede a promociones exclusivas.</li>
                    </ul>
                </nav>
            </div>
        </div>
        
    </section>
    
    <?php include("content-aside.php"); ?>
    
</main>


<?php include("footer.php"); ?>