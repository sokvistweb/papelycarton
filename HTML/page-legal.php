<?php
$bodyClass = 'legal';
include 'header.php';
?>


<section class="billboard">
    
</section>


<main>
    
    <section class="categories-section container">
        
        <div class="row">
            <div class="col">
                <h1>Política de privacidad</h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col page-content">
                
                <h2>Declaración de protección de datos</h2>
                <p>Nos complace mucho que haya visitado nuestra página web y que muestre su interés por VEGIO, S.L. y nuestros servicios. Para que usted pueda sentirse seguro y cómodo mientras navega por nuestra página web, nos tomamos muy en serio la protección de sus datos personales y el tratamiento confidencial de los mismos.</p>
                <p>Con las presentes informaciones sobre protección de datos deseamos mantenerle al corriente de cuándo guardamos qué datos y para qué fin los utilizamos, naturalmente cumpliendo en todo momento la normativa vigente en materia de protección de datos.</p>
                <p>El desarrollo tecnológico continuo, las modificaciones de nuestros servicios o de la normativa así como otros motivos pueden originar adaptaciones en nuestra información relativa a la protección de datos. Por ello nos reservamos el derecho de modificar en cualquier momento la presente declaración de protección de datos y rogamos se informe regularmente del estado actualizado de la misma.</p>
                <h2>Información básica</h2>
                <p>Salvo disposición contraria en los siguientes apartados, no se recogerán, procesarán o utilizarán por lo general datos personales algunos al usar nuestras páginas web.</p>
                <p>Al acceder a nuestras páginas web, nuestros servidores recogerán automáticamente informaciones de carácter general. Éstas incluyen el tipo de navegador web, el sistema operativo utilizado, el nombre de dominio del proveedor de servicios de internet, la dirección IP del ordenador utilizado, la página web desde la cual nos visita, las páginas nuestras que usted visita así como la fecha y la duración de la visita.</p>
                <p>Dichos datos no podrán ser utilizados por nosotros para identificar a cada usuario. En cambio, sólo evaluaremos estadísticamente las informaciones y las destinaremos exclusivamente a mejorar el atractivo, los contenidos y las funciones de nuestras páginas web.</p>
                <h2>Concepto de datos personales </h2>
                <p>El Reglamento General de Protección de Datos (RGPD) de la Unión Europea define los datos personales del modo siguiente:</p>
                <p>Toda información relacionada con una persona física identificada o identificable (en adelante “el interesado”); se considerará persona física identificable toda persona cuya identidad pueda determinarse, directa o indirectamente, en particular mediante un identificador, como por ejemplo un nombre, un número de identificación, datos de localización, un identificador en línea o uno o varios elementos propios de la identidad física, fisiológica, genética, psíquica, económica, cultural o social de dicha persona. </p>
                <p><strong>Base legal de la recogida y del procesamiento de datos personales: </strong></p>
                <p>En caso de solicitar su autorización para procesar datos personales, la base legal para ello será el Art. 6 ap. 1 letra a RGPD.</p>
                <p>Al procesar sus datos personales para dar cumplimiento a un contrato entre usted y VEGIO, S.L., se tomará como base legal el Art. 6 ap. 1 letra b RGPD. Ello será también de aplicación para el procesamiento de datos personales necesario para llevar a cabo medidas precontractuales. </p>
                <p>En caso de ser necesario el procesamiento de datos personales para cumplir con una obligación legal a la cual esté sometida nuestra empresa, la base legal será el Art. 6 ap. 1 letra c RGPD.</p>
                <p>Si el procesamiento es necesario para salvaguardar un interés justificado de nuestra empresa o de un tercero y si los intereses y los derechos y libertades fundamentales del interesado no priman sobre el interés anteriormente mencionado, la base legal para dicho procesamiento será el Art. 6 ap. 1 letra f RGPD.</p>
                <p><strong>Eliminación de datos y duración del almacenamiento: </strong></p>
                <p>Los datos personales del interesado serán eliminados o bloqueados así que deje de darse la finalidad del almacenamiento de los mismos. Asimismo, podrá producirse un almacenamiento de datos si así lo prevé el legislador europeo o nacional en reglamentos comunitarios, leyes u otras normas a las que esté sometido el interesado. También se bloquearán o eliminarán los datos si caduca el plazo de almacenamiento previsto por dichas normas, salvo si se da la exigencia de continuar almacenando los datos para la conclusión o el cumplimiento de un contrato.</p>
                <h2>Recogida y procesamiento de datos personales</h2>
                <p>Solamente se recogerán datos personales si usted los facilita a instancias propias, por ejemplo para realizar un pedido, solicitar información, dar el servicio o actividades relacionadas con el mismo o darse de alta en servicios personalizados. En caso de enviarle publicidad se le informará del fin pretendido del procesamiento y en caso necesario se le solicitará su autorización a almacenar los datos.</p>
                <p>Los datos personales recogidos en nuestras páginas web se utilizarán sin su consentimiento solamente para cursar pedidos o tramitar sus consultas. Asimismo, sus datos se utilizarán solamente con su consentimiento en una base de datos centralizada de clientes e interesados gestionada bajo responsabilidad de VEGIO, S.L.. El uso de dichos datos se limitará a fines publicitarios y estudios de mercado y opinión. Usted podrá revocar en todo momento su consentimiento para el futuro.</p>
                <p>Sus datos y su procesamiento quedarán protegidos del acceso por personas no autorizadas. Recuerde que los correos electrónicos no encriptados no se transmitirán protegidos contra acceso no autorizado.</p>
                <p>Sus datos no serán vendidos, alquilados o cedidos a terceros de otro modo que no sea el descrito aquí. La transmisión de datos personales a instituciones y autoridades públicas sólo se realizará de acuerdo con la normativa nacional vinculante. Nuestros trabajadores, agentes y distribuidores están obligados por nosotros a mantener la más estricta confidencialidad.</p>
                <h2>Formulario de contacto </h2>
                <p>Usted tiene la posibilidad de contactarnos a través de nuestra dirección de correo electrónico o el formulario de contacto. Nosotros utilizaremos los datos personales que nos facilite por esta vía exclusivamente para el fin para el cual nos los facilita al contactarnos.</p>
                <p>Si en nuestro formulario de contacto solicitamos información que no es necesaria para tomar contacto con nosotros, la identificaremos en todo momento como opcional. Dicha información nos servirá para concretar su consulta y tramitarla de la mejor manera posible. La facilitación de dicha información será explícitamente voluntaria y con su consentimiento. Si se trata de información sobre canales de comunicación (correo electrónico, teléfono, etc.), usted también prestará su consentimiento a que le contactemos a través de dicho canal, si procede, para poder responder a su consulta.</p>
                <p>Usted podrá revocar su consentimiento en cualquier momento futuro.  </p>
                <p> </p>
                <h2>Portal de usuarios </h2>
                <p>Usted tiene la posibilidad de registrarse en nuestra página web y crear una cuenta como usuario. Para el registro recogeremos y almacenaremos los datos suyos (opcionales) que figuran a continuación:</p>
                <p>Empresa</p>
                <ul>
                <li>Nombre de empresa</li>
                <li>Nombre</li>
                <li>Apellidos</li>
                <li>Dirección Completa Fiscal</li>
                <li>Teléfono(s)</li>
                <li>NIF</li>
                <li>E-mail / (nombre de usuario)</li>
                </ul>
                <p>Una vez realizado el registro, usted recibirá un acceso personalizado protegido con contraseña, donde podrá acceder y administrar sus datos facilitados. El registro será voluntario pero puede ser condición para poder utilizar nuestros servicios.</p>
                <h2>Boletín para clientes / Newsletter </h2>
                <p>Usted puede suscribirse a nuestro boletín para clientes. Para suscribirse a nuestro boletín utilizamos el llamado opt-in doble. Ello significa que una vez usted haya indicado su dirección de correo electrónico, le enviaremos a ésta un mensaje de confirmación, en el que le solicitamos que nos confirme que desea recibir el boletín. Si así lo confirma, guardaremos su dirección de correo electrónico hasta que se dé de baja del boletín. El único fin de dicho almacenamiento es poderle enviar el boletín. Asimismo, guardaremos sus direcciones IP al darse de alta y confirmar la suscripción así como el momento de dichas acciones para evitar un uso fraudulento de sus datos personales.</p>
                <p>La única información obligatoria para recibir el boletín es la dirección de correo electrónico. La demás información indicada de forma separada es voluntaria y se utilizará solamente para personalizar el boletín. Estos datos también se eliminarán completamente en caso de revocación.</p>
                <p>Usted podrá revocar en todo momento su consentimiento al envío del boletín. Podrá declarar su revocación haciendo clic en el enlace indicado en cada mensaje del boletín o bien utilizando los datos de contacto del delegado de protección de datos indicados a continuación. Los datos que usted facilite no se transmitirán a terceros.</p>
                <h2>Transmisión de datos a proveedores de servicios externos (encargados del tratamiento)</h2>
                <p>Sus datos serán facilitados a proveedores de servicios externos encargados por nosotros que prestan apoyo a VEGIO, S.L. en la prestación de sus servicios.</p>
                <p>El tratamiento de sus datos personales por proveedores de servicios encargados se realizará dentro del tratamiento por encargo según el Art. 28 RGPD.</p>
                <p>Los mencionados proveedores de servicios solamente tendrán acceso a aquella información personal que sea necesaria para llevar a cabo la correspondiente actividad. Estará prohibido a dichos proveedores de servicios transmitir su información personal o utilizarla para fines diferentes, especialmente fines publicitarios propios.</p>
                <p>En caso de que los proveedores de servicios externos accedan a sus datos personales, nosotros garantizaremos a través de medidas legales, técnicas y organizativas así como mediante controles regulares que ellos también cumplan la normativa vigente en materia de protección de datos.</p>
                <p>No se realizará ninguna transmisión de sus datos personales a otras empresas con fines comerciales.</p>
                <h2>Herramientas de optimización de páginas web</h2>
                <p>En la recogida de datos personales a través de herramientas de optimización de páginas web nos remitimos a nuestro interés legítimo según el Art. 6 ap. 1 letra f RGPD en combinación con el considerando nº 47. De acuerdo con éstos, la publicidad directa representa normalmente un interés legítimo. Sus intereses y derechos y libertades fundamentales no primarán sobre nuestro interés publicitario, dado que en nuestra declaración de protección de datos le informamos exhaustivamente de la recogida de datos y usted dispone en todo momento de la opción de exclusión (mediante enlace o configuración del navegador). Asimismo, solamente hacemos uso de un tracking pseudonimizado.</p>
                <h2>Cookies</h2>
                <p>En determinadas partes de nuestras páginas se utilizan cookies. Una cookie es un pequeño archivo de texto que se baja de una página web en su disco duro. Las cookies no dañan su ordenador y no contienen virus. Las cookies de nuestras páginas web no recogen datos personales algunos. Nosotros utilizamos la información contenida en las cookies para facilitarle el uso de nuestras páginas y adaptarlas a sus necesidades.</p>
                <p>Nuestra página utiliza tanto cookies de sesión como cookies permanentes. Las cookies de sesión son cookies temporales que se guardan en el navegador de internet del usuario hasta que se cierra la ventana del navegador y se borran las cookies de sesión. Las cookies permanentes se utilizan para visitas repetidas, guardándose en el navegador del usuario durante un tiempo determinado (normalmente 1 año o más). Dichas cookies no se borran al cerrar el navegador. Este tipo de cookies se utiliza para recordar las preferencias de un usuario cuando vuelve a la página.</p>
                <p>Usted también puede utilizar nuestra página web sin cookies. Si no desea que se guarden cookies en su ordenador, puede desactivar dicha opción en la configuración de su navegador. Usted podrá borrar en todo momento las cookies guardadas en la configuración de su navegador. Las instrucciones del fabricante de su navegador le explicarán cómo funciona en cada caso. Sin embargo, recordamos que la oferta de nuestra web puede estar limitada si se desactivan las cookies.</p>
                <h2>Seguridad de los datos</h2>
                <p>VEGIO, S.L. emprende medidas de seguridad técnicas y organizativas para proteger sus datos gestionados por nosotros contra manipulación, pérdida o destrucción intencionada o no intencionada o acceso por personas no autorizadas. Nuestras medidas de seguridad se mejoran de forma continua de acuerdo con el progreso tecnológico.</p>
                <h3>Derecho de los interesados</h3>
                <p>Al procesarse datos personales suyos, usted es un interesado según el RGPD, por lo que le corresponden los siguientes derechos frente al responsable: </p>
                <ul>
                <li>Derecho de acceso según Art. 15 RGPD</li>
                </ul>
                <p>Usted podrá solicitarnos una confirmación de si nosotros tratamos datos personales que le afecten. En caso de haber tratado datos suyos, usted disfrutará de derechos de información adicionales según el Art. 15 RGPD.</p>
                <ul>
                <li>Derecho de rectificación</li>
                </ul>
                <p>Si los datos suyos que hemos recogido son incorrectos o incompletos, usted podrá solicitarnos que se rectifiquen sin dilación, de acuerdo con el Art. 16 RGPD.</p>
                <ul>
                <li>Derecho a limitación del tratamiento</li>
                </ul>
                <p>En virtud de lo dispuesto en el Art. 18 RGPD, usted podrá solicitar bajo determinadas circunstancias la limitación del tratamiento de los datos personales que le afecten.</p>
                <p>De acuerdo con la limitación, sus datos sólo podrán ser tratados con su consentimiento o para hacer valer, ejercer o defender derechos legales o para proteger los derechos de otra persona física o jurídica o por motivos de un interés público importante de la Unión o de un estado miembro. Antes de levantar la limitación, le informaremos debidamente.</p>
                <ul>
                <li>Derecho de supresión</li>
                </ul>
                <p>En caso de prevalecer uno de los motivos según el Art. 17 ap. 1 RGPD, usted podrá solicitarnos la supresión inmediata de los datos personales que le afecten, salvo si existe una excepción de la obligación de supresión según el Art. 17 ap. 3 RGPD.</p>
                <ul>
                <li>Derecho de notificación</li>
                </ul>
                <p>Si usted ha hecho valer frente a nosotros el derecho a rectificación, supresión o limitación del tratamiento, estaremos obligados según el Art. 19 RGPD a notificarlo a todos los destinatarios de sus datos personales, salvo si la notificación resulta imposible o exige un esfuerzo desproporcionado. Usted tendrá asimismo el derecho a ser informado de los destinatarios. Ante el responsable tendrá derecho a ser informado de dichos destinatarios.</p>
                <ul>
                <li>Derecho de portabilidad de datos</li>
                </ul>
                <p>Asimismo, de acuerdo con el Art. 20 RGPD, usted tiene derecho a obtener de nosotros los datos personales que le afecten en un formato apto para lectura mecánica y a transmitir los datos a otro responsable del tratamiento sin impedimento alguno, siempre y cuando se cumplan los requisitos del Art. 20 ap. 1 letra a RGPD, o bien instarnos a transmitir sus datos personales directamente a otro responsable del tratamiento, siempre y cuando ello sea técnicamente viable y no perjudique los derechos y libertades de otras personas. Este derecho no será de aplicación para un tratamiento de datos personales necesario para cumplir una función de interés público o para ejercer un poder público.</p>
                <ul>
                <li>Derecho de oposición</li>
                </ul>
                <p>Usted tiene el derecho de oponerse en todo momento ante VEGIO, S.L. al tratamiento de los datos que le afecten, de acuerdo con el Art. 6 ap. 1 letra f RGPD.</p>
                <p>Nosotros dejaremos de tratar sus datos personales, salvo si existen motivos dignos de protección para el tratamiento que primen sobre sus intereses, derechos y libertades o si el tratamiento sirve para hacer valer, ejercer o defender derechos legales.</p>
                <ul>
                <li>Derecho de revocación de la declaración de autorización según la normativa de protección de datos</li>
                </ul>
                <p>Usted tendrá en todo momento el derecho de revocar su declaración de autorización según la normativa de protección de datos mediante declaración ante VEGIO, S.L.. La revocación de la autorización no afectará la legalidad del tratamiento llevado a cabo en virtud de la autorización hasta su revocación.</p>
                <ul>
                <li>Derecho de queja ante la autoridad de control</li>
                </ul>
                <p>Sin perjuicio de otros recursos administrativos o judiciales, usted tendrá en todo momento derecho de queja ante una autoridad de control, en especial en el estado miembro de su lugar de residencia, su lugar de trabajo o el lugar de la presunta infracción, si usted considera que el tratamiento de los datos personales que le afectan infringen dicho Reglamento. </p>
                <p> </p>
                <h2>Modificaciones de la declaración de protección de datos</h2>
                <p>La presente declaración de protección de datos se adaptará de forma continua a medida que se desarrolle internet o nuestra oferta. Las modificaciones se darán a conocer en esta página con la debida antelación. Para estar informado del estado actual de nuestras disposiciones en materia de uso de datos, recomendamos consultar regularmente esta página.</p>
                
            </div>
        </div>
        
    </section>
    
    <?php include("content-aside.php"); ?>
    
</main>


<?php include("footer.php"); ?>