<?php
$bodyClass = 'home';
include 'header.php';
?>


<section class="billboard">
    <h1>Apuesta por la sostenibilidad,<br> crea con nuestro cartón</h1>
    <div class="overlay"></div>
</section>


<main>
    
    <section class="features" id="features">
        
        <span><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-albums"></use></svg>Nuestras calidades</span>
        
        <div class="row">
            <div class="col">
                <div class="features-content">
                    <h3>Corte a medida</h3>
                </div>
            </div>

            <div class="col">
                <div class="features-content">
                    <h3>Cantidades según tus necesidades</h3>
                </div>
            </div>

            <div class="col">
                <div class="features-content">
                    <h3>100% fibras recicladas</h3>
                    <svg class=""><use xlink:href="assets/images/icons/symbol-defs.svg#icon-recycle"></use></svg>
                </div>
            </div>
        </div>
    </section>
    
    <section class="efficiency wrapper">
        
        <svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-info"></use></svg>
        
        <h2>Nuestra eficiencia empieza por comprender tus necesidades</h2>
        
        <a href="page-help.php" class="button red float-right" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
    
    </section>
    
    <aside>
        
        <div class="row cards">
            <div class="col">
                <div class="card-body">
                    <svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-samples"></use></svg>
                    <h4>¿Te enviamos una caja de muestras?</h4>
                    <img src="assets/images/muestrario.png" width="128" height="100">
                </div>
                <div class="card-footer">
                    <a href="#" class="button small" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
                </div>
            </div>

            <div class="col">
                <div class="card-body">
                    <svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-idea"></use></svg>
                    <h4>Ideas en cartón</h4><img src="assets/images/ideas.png" width="121" height="150">
                </div>
                <div class="card-footer">
                    <a href="page-blog.php" class="button small" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
                </div>
            </div>

            <div class="col">
                <div class="card-body">
                    <svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-percent"></use></svg>
                    <h4>Stocks</h4>
                    <p>Ven a ver las ofertas y liquidaciones de papel y cartón. Ti te suscribes al newsletter te informaremos antes que nadie.</p>
                </div>
                <div class="card-footer">
                    <a href="page-members.php" class="button small" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
                </div>
            </div>
        </div>
        
        <div class="tellus">
            <div class="tellus-wrapper">
                
                <svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-info"></use></svg>
        
                <span>Conectamos contigo</span>

                <h3>Dinos qué haces y te diremos qué cartón necesitas</h3>
                
                <a href="page-help.php" class="button red float-right" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
                
            </div>
        </div>
    </aside>
    
</main>


<?php include("footer.php"); ?>