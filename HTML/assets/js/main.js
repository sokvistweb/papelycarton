/* =============================================================================
   jQuery: all the custom stuff!
   ========================================================================== */

jQuery(document).ready(function($){
    
    
    /*  Animated header */
    var changeHeader = 160;
    $(window).scroll(function() {
        var scroll = getCurrentScroll();
        if ( scroll >= changeHeader ) {
            $('body').addClass('scrolled');
            }
            else {
                $('body').removeClass('scrolled');
            }
    });
    
    function getCurrentScroll() {
        return window.pageYOffset;
    }
    
    
    /* open/close products panel *****/
	$('.dropdown-trigger').on('click', function(event){
		event.preventDefault();
		toggleNav();
	});

	// close panel
	$('.products-dropdown .cd-close').on('click', function(event){
		event.preventDefault();
		toggleNav();
	});

	function toggleNav(){
		var navIsVisible = ( !$('.products-dropdown').hasClass('dropdown-is-active') ) ? true : false;
		$('.products-dropdown').toggleClass('dropdown-is-active', navIsVisible);
		$('.dropdown-trigger').toggleClass('dropdown-is-active', navIsVisible);
		
	}
    
    
    /*  Fixed side bar open/close *****/
    var open = false;

    var openSidebar = function(){
        $('.sidebar').addClass("opened");
        $('.toggle-btn').addClass('toggle');
        open = true;
    }
    var closeSidebar = function(){
        $('.sidebar').removeClass("opened");
        $('.toggle-btn').removeClass('toggle');
        open = false;
    }

    $('.toggle-btn').click( function(event) {
        event.stopPropagation();
        var toggle = open ? closeSidebar : openSidebar;
        toggle();
    });

    $(document).click( function(event){
        if ( !$(event.target).closest('.sidebar').length ) {
            closeSidebar();   
        }
    });
    
    
    
    /* mgAccordion https://github.com/marqusG/mgAccordion *****/ 
    $('.products-list').mgaccordion({
        //theme: 'tree',
        leaveOpen: false
    });
    
    
    
    // Count and show child elements
    $('.dropdown').each(function(){
        var numChilds = $('li a', $(this)).length;
        $('.toggler', $(this)).html(numChilds);
    });
    
    
    
    // Show/hide fields on help form
    // https://www.tutorialrepublic.com/faq/show-hide-divs-based-on-radio-button-selection-in-jquery.php
    $('.radio-client input[type="radio"]').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        //$(".box").not(targetBox).hide();
        //$(targetBox).show();
        $(".box-fields").not(targetBox).removeClass('togglefield');
        $(targetBox).addClass('togglefield');
    });
    
    
});

