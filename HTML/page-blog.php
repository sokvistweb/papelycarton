<?php
$bodyClass = 'blog';
include 'header.php';
?>


<section class="billboard">
    
</section>


<main>
    
    <section class="blog-section container">
        
        <div class="row">
            <div class="col">
                <h1>Ideas en cartón</h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col col-9">
                <div class="posts_list">
                    <ul class="post-list">
                        <li class="post-card">
                            <a href="page-blog-single.php"><img width="900" height="600" src="assets/images/blog/problemas_ii_2.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></a>
                            
                            <h2><a href="page-blog-single.php">Dificultades principales que se pueden encontrar al trabajar un cartón</a></h2>
                            
                            <div class="entry-meta">
                                <span class="post-date"><time datetime="2020-02-24 15:19">22 de julio de 2020</time></span>
                            </div>
                            <div class="entry-content clearfix">
                                Maecenas rhoncus justo aliquam urna finibus tincidunt et ut mauris. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            </div>
                            
                            <div class="btn-wrapper">
                                <a href="page-blog-single.php" class="button small" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
                            </div>
                        </li>
                        <li class="post-card">
                            <a href="page-blog-single.php"><img width="900" height="600" src="assets/images/blog/mussol-1.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></a>
                            
                            <h2><a href="page-blog-single.php">Figuras de papel para montárselo bien</a></h2>
                            
                            <div class="entry-meta">
                                <span class="post-date"><time datetime="2020-02-24 15:19">12 de junio de 2020</time></span>
                            </div>
                            <div class="entry-content clearfix">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc et sagittis dui. Maecenas rhoncus justo aliquam urna.
                            </div>
                            
                            <div class="btn-wrapper">
                                <a href="page-blog-single.php" class="button small" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
                            </div>
                        </li>
                        <li class="post-card">
                            <a href="page-blog-single.php"><img width="900" height="600" src="assets/images/blog/papel_vegetal.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></a>
                            
                            <h2><a href="page-blog-single.php">Cartón reciclado: una tendencia por la protección del medio ambiente</a></h2>
                            
                            <div class="entry-meta">
                                <span class="post-date"><time datetime="2020-02-24 15:19">24 de mayo de 2020</time></span>
                            </div>
                            <div class="entry-content clearfix">
                                Nunc et sagittis dui. Maecenas rhoncus justo aliquam urna finibus tincidunt et ut mauris.
                            </div>
                            
                            <div class="btn-wrapper">
                                <a href="page-blog-single.php" class="button small" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
                            </div>
                        </li>
                        <li class="post-card">
                            <a href="page-blog-single.php"><img width="900" height="600" src="assets/images/blog/papel_reciclado_2_baja.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></a>
                            
                            <h2><a href="page-blog-single.php">Papeles especiales</a></h2>
                            
                            <div class="entry-meta">
                                <span class="post-date"><time datetime="2020-02-24 15:19">7 de abril de 2020</time></span>
                            </div>
                            <div class="entry-content clearfix">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc et sagittis dui. Maecenas tincidunt et ut mauris.
                            </div>
                            
                            <div class="btn-wrapper">
                                <a href="page-blog-single.php" class="button small" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
                            </div>
                        </li>
                    </ul>
                </div>
                
                <nav class="pagination">
                    <ul>
                        <li class="active"><span aria-current="page" class="page-numbers current">1</span></li>
                        <li><a href="#" class="page">2</a></li>
                        <li><a href="#">&#10140;</a></li>
                    </ul>
                </nav>
            </div>
            
            <div class="col col-3">
                <div id="categories-3" class="widget widget_categories">
                    <h3>Categorias</h3>
                    <ul>
                        <li class="cat-item cat-item-59"><a href="page-blog.php">Técnica</a></li>
                        <li class="cat-item cat-item-61"><a href="page-blog.php">Tendencias</a></li>
                        <li class="cat-item cat-item-80"><a href="page-blog.php">Medio ambiente</a></li>
                        <li class="cat-item cat-item-86"><a href="page-blog.php">Novedades</a></li>
                        <li class="cat-item cat-item-53"><a href="page-blog.php">Ideas</a></li>
                    </ul>
                </div>
                <div id="recent-posts-3" class="widget widget_recent_entries">
                    <h3>Entradas recientes</h3>
                    <ul>
				        <li><a href="page-blog.php">¿Como es el proceso de fabricación del cartón?</a></li>
						<li><a href="page-blog.php">Papeles especiales</a></li>
                        <li><a href="page-blog.php">Cartón reciclado: una tendencia por la protección del medio ambiente</a></li>
                        <li><a href="page-blog.php">Figuras de papel para montárselo bien</a></li>
                        <li><a href="page-blog.php">Dificultades principales que se pueden encontrar al trabajar un cartón</a></li>
					</ul>
                </div>
            </div>
        </div>
        
    </section>

    
    <?php include("content-aside.php"); ?>
    
</main>


<?php include("footer.php"); ?>