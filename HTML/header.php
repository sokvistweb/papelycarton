<!DOCTYPE html>
<html>
<head>
    <title>papel y cartón - Apuesta por la sostenibilidad, crea con nuestro cartón</title>
    <meta charset="utf-8">
    <meta name="author" content="">
	<meta name="description" content="Cartón, cartoncillo y cartón ondulado a su medida, 100% reciclado."/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png" /><!-- 57×57px -->
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png"><!-- 180×180px -->

    <link href="https://fonts.googleapis.com/css2?family=Merriweather:ital,wght@1,900&family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
    
    <!-- Open Graph -->
    <meta property="og:locale" content="es-ES">
    <meta property="og:type" content="website">
    <meta property="og:title" content="papel y cartón - apuesta por el biodegradable, crea con cartón">
    <meta property="og:description" content="Cartón, cartoncillo y cartón ondulado a su medida, 100% reciclado.">
    <meta property="og:image" content="assets/images/og-image.jpg">
    <meta property="og:url" content="https://papelycarton.com/">
    <meta property="og:site_name" content="papel y cartón">

</head>

<body class="<?php echo $bodyClass ?>">

	<header>
        <div class="logo">
            <a href="index.php" rel="home">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 600 122"><path d="M28.3 31c3.1-1.9 6.5-2.9 10.2-2.9 6.1 0 10.8 2.2 14 6.5 3.2 4.3 4.8 10.1 4.8 17.4 0 6.1-1.4 12.3-4.3 18.7-2.8 6.4-6.9 11.7-12.2 16-5.3 4.2-11.5 6.3-18.4 6.3-2.2 0-5.1-.5-8.9-1.4l-3.9 28.8H1l11.3-84.2-6-2.2.8-3.6L19.5 28l2.1 1.1-1.1 9c2.1-2.8 4.7-5.2 7.8-7.1zm-4.6 56.5c5 0 9.4-1.8 13-5.4 3.7-3.6 6.5-8.1 8.4-13.5C47 63.2 48 57.8 48 52.5c0-5.7-1-10.1-3-13.2-2-3.1-4.7-4.7-8.1-4.7-3.4 0-6.7 1.1-9.8 3.3-3.1 2.2-5.5 4.7-7.2 7.5L14.4 85c2.2 1.7 5.3 2.5 9.3 2.5zM105.7 83.6c0 1.2.2 2.1.7 2.6.5.5 1 .8 1.5.8.7 0 1.5-.2 2.2-.7.7-.5 1.6-1.3 2.8-2.4l1.6 2.5c-.3.6-1 1.4-2.1 2.5s-2.4 2.1-4 2.9c-1.6.9-3.2 1.3-4.9 1.3-4.6 0-6.8-2.5-6.5-7.4l.7-4.6c-2.5 3.3-5.5 6.1-8.9 8.5-3.4 2.4-6.9 3.5-10.6 3.5-5.9 0-10.3-2.1-13.2-6.4-2.9-4.3-4.3-10-4.3-17.3 0-6.1 1.4-12.3 4.3-18.7 2.8-6.4 6.9-11.8 12.3-16.1 5.4-4.3 11.6-6.5 18.7-6.5 1.6 0 3.4.2 5.5.6 2.1.4 4 .8 5.6 1.3l6.2-1.8-7.3 54.1c-.2.3-.3.8-.3 1.3zm-11.1-50c-5.2 0-9.6 1.8-13.4 5.3-3.7 3.6-6.6 8-8.5 13.4-1.9 5.4-2.9 10.7-2.9 16 0 5.8.8 10.2 2.5 13.4 1.7 3.2 4.3 4.8 7.7 4.8 3.4 0 6.7-1.3 10.1-4 3.3-2.7 6.1-5.6 8.4-8.8l4.9-37.6c-2.4-1.7-5.4-2.5-8.8-2.5zM144 31c3.1-1.9 6.5-2.9 10.2-2.9 6.1 0 10.8 2.2 14 6.5 3.2 4.3 4.8 10.1 4.8 17.4 0 6.1-1.4 12.3-4.3 18.7-2.8 6.4-6.9 11.7-12.2 16-5.3 4.3-11.5 6.4-18.4 6.4-2.2 0-5.1-.5-8.9-1.4l-3.9 28.8h-8.5l11.3-84.2-6-2.2.8-3.6 12.4-2.4 2.1 1.1-1.1 9c2.1-2.9 4.7-5.3 7.7-7.2zm-4.6 56.5c5 0 9.4-1.8 13-5.4 3.7-3.6 6.5-8.1 8.4-13.5s2.9-10.8 2.9-16.1c0-5.7-1-10.1-3-13.2-2-3.1-4.7-4.7-8.1-4.7-3.4 0-6.7 1.1-9.8 3.3-3.1 2.2-5.5 4.7-7.2 7.5L130.1 85c2.3 1.7 5.4 2.5 9.3 2.5zM212 89.3c-4.2 2.5-8.8 3.7-13.8 3.7-4.9 0-9-1.1-12.2-3.4-3.2-2.2-5.6-5.2-7.1-8.8-1.5-3.6-2.3-7.6-2.3-11.9 0-7 1.4-13.7 4.3-20 2.8-6.3 6.7-11.3 11.6-15.2 4.9-3.8 10.4-5.7 16.3-5.7 5.1 0 9 1.3 11.6 3.8 2.6 2.5 3.9 5.8 3.9 9.9 0 5.3-2.2 9.8-6.7 13.4-4.5 3.6-9.8 6.4-15.8 8.2-6.1 1.8-11.3 2.8-15.7 2.9-.1.5-.1 1.4-.1 2.6 0 4.8 1.1 9 3.4 12.5 2.2 3.5 5.7 5.3 10.3 5.3 3.2 0 6.6-.7 10-2.2 3.4-1.5 6.6-3.7 9.6-6.6l2.1 3.8c-2.1 2.7-5.2 5.2-9.4 7.7zm-15.5-51.6c-3 2.9-5.4 6.5-7.1 10.8-1.8 4.3-2.8 8.5-3.2 12.6 4.3-.1 8.7-1 13.2-2.5s8.3-3.7 11.4-6.5c3.1-2.8 4.6-6 4.6-9.7-.1-3-.9-5.3-2.4-6.9-1.5-1.6-3.6-2.4-6.3-2.4-3.8.3-7.2 1.7-10.2 4.6zM240 84.1c0 .9.1 1.6.4 2.1.3.5.7.7 1.1.7 1.6 0 3.6-1.2 6-3.7l1.5 2.5c-.8 1.5-2.3 3.1-4.4 4.8-2.1 1.7-4.6 2.5-7.5 2.5-3.9 0-5.8-2.1-5.8-6.4l.3-4.2L242 8.9l-6.7-1.8.9-3.8 13-1.9 2.1 1.3L240.1 83l-.1 1.1z"/><path class="and" d="M256.2 38.5c-.3-.4-.8-.7-1.4-.7-1 0-1.9.3-2.8.9l-1.7-4.3c1-1.2 2.8-2.6 5.6-4.1 2.7-1.5 5.6-2.3 8.7-2.3 3.2 0 5.7.6 7.3 1.9 1.6 1.3 2.7 3.1 3 5.6l6.9 43.1 1.6 10.9L287 82c5.8-12.6 8.8-22.9 8.8-31 0-1.7-.1-3.2-.4-4.4-.3-1.2-.7-2.5-1.2-3.9-.5-1.3-.9-2.4-1.1-3.4-.2-1-.3-2.3-.3-3.8 0-2.2.7-4 2.2-5.4 1.5-1.4 3.6-2.1 6.4-2.1 3.1 0 5.5 1.1 7.2 3.4 1.8 2.3 2.6 4.9 2.6 7.9 0 5.3-.7 10.1-2.1 14.4-1.4 4.3-3.7 9.7-6.8 16.1-2.8 6.1-9.4 17.7-19.9 34.6-3.6 6-7.3 10.1-11 12.4-3.7 2.2-8.1 3.4-12.9 3.4-2.2 0-4.5-.3-6.9-.8-2.4-.5-3.9-1.1-4.6-1.7l4.2-11c.8.8 2.3 1.6 4.4 2.2 2.1.7 4 1 5.8 1 3.5 0 6.7-1.1 9.6-3.3 2.8-2.2 5.8-5.9 9-11h-10l-12.8-54.9c-.3-.9-.6-1.7-1-2.2z"/><g><path d="M344.2 35.9c-1.8-.8-3.6-1.2-5.2-1.2-4.2 0-7.9 1.4-11 4.1-3.1 2.7-5.6 6.5-7.2 11.4s-2.5 10.5-2.5 16.9c0 6.4 1.1 11.3 3.4 14.6 2.3 3.3 5.5 4.9 9.6 4.9 3.1 0 5.7-.6 8-1.7s4.9-2.8 7.8-5.2l1.8 3.7c-1.9 2.4-4.6 4.6-8 6.6-3.4 2-7.5 3-12.2 3-6.2 0-11.1-2.2-14.6-6.7s-5.2-10.6-5.2-18.4c0-6 1.3-12.1 3.8-18.3 2.5-6.2 6.3-11.3 11.1-15.4 4.9-4.1 10.6-6.2 17.2-6.2 2.1 0 4.3.2 6.6.7 2.3.5 4.2 1.1 5.7 2l-2.6 9-1.2-.9c-1.7-1.1-3.4-2.1-5.3-2.9zM397.5 83.6c0 1.2.2 2.1.7 2.6.5.5 1 .8 1.5.8.7 0 1.5-.2 2.2-.7.7-.5 1.6-1.3 2.8-2.4l1.6 2.5c-.3.6-1 1.4-2.1 2.5s-2.4 2.1-4 2.9c-1.6.9-3.2 1.3-4.9 1.3-4.6 0-6.8-2.5-6.5-7.4l.7-4.6c-2.5 3.3-5.5 6.1-8.9 8.5-3.4 2.4-6.9 3.5-10.6 3.5-5.9 0-10.3-2.1-13.2-6.4-2.9-4.3-4.3-10-4.3-17.3 0-6.1 1.4-12.3 4.3-18.7 2.8-6.4 6.9-11.8 12.3-16.1 5.4-4.3 11.6-6.5 18.7-6.5 1.6 0 3.4.2 5.5.6 2.1.4 4 .8 5.6 1.3l6.2-1.8-7.3 54.1c-.2.3-.3.8-.3 1.3zm-11.1-50c-5.2 0-9.6 1.8-13.4 5.3-3.7 3.6-6.6 8-8.5 13.4-1.9 5.4-2.9 10.7-2.9 16 0 5.8.8 10.2 2.5 13.4 1.7 3.2 4.3 4.8 7.7 4.8 3.4 0 6.7-1.3 10.1-4 3.3-2.7 6.1-5.6 8.4-8.8l4.9-37.6c-2.4-1.7-5.3-2.5-8.8-2.5zM432.6 36.9c2.1-2.4 4.6-4.5 7.5-6.2 2.9-1.7 5.9-2.5 9.2-2.5 1.8 0 3 .2 3.6.7l-2 9.8c-1.1-.6-2.8-.9-5.2-.9-3.4 0-6.9 1.3-10.5 3.9-3.5 2.6-6.3 6.3-8.2 11l-4.8 39.2h-8.8l7.1-54.6-5.7-2.6.8-4 11.8-2.4 2.1 1-.8 10-.7 4.6c1-2.2 2.5-4.6 4.6-7zM463.4 81.3c0 1.9.4 3.3 1.2 4.1.8.8 2.2 1.2 4.2 1.2 1.5 0 3.4-.5 5.7-1.6 2.3-1 4.1-2.1 5.4-3.1l.9 3.5c-1.4 1.7-3.8 3.4-7.2 5.1-3.4 1.7-6.6 2.5-9.6 2.5-6.4 0-9.7-3.1-9.7-9.2 0-1.9.2-4.3.6-7.1l5.3-41h-6.9l1-3.9 6.9-2.1c1.6-2 3.7-7.2 6.3-15.4h4.4l-2 15.3h15.5l-.9 6.2h-15.3L464.1 73c-.5 4.3-.7 7.1-.7 8.3zM532 34.9c3.7 4.6 5.6 11 5.6 19.2 0 6.6-1.3 12.9-4 18.8-2.7 6-6.4 10.8-11.1 14.5-4.8 3.7-10.1 5.6-16.1 5.6-6.9 0-12.2-2.2-16.1-6.6-3.8-4.4-5.8-10.6-6-18.6-.1-6.7 1.2-13.2 3.9-19.3 2.7-6.1 6.4-11 11.2-14.8 4.8-3.8 10.2-5.7 16.3-5.7 7.2.1 12.6 2.4 16.3 6.9zm-28.9 3.7c-3.1 3.4-5.5 7.6-7 12.8-1.6 5.2-2.4 10.4-2.4 15.6 0 6.6 1.2 11.7 3.7 15.2s6 5.3 10.7 5.3c4.2 0 7.8-1.7 10.9-5 3.1-3.3 5.4-7.5 7-12.6 1.6-5.1 2.4-10.2 2.4-15.4 0-6.7-1.2-11.9-3.5-15.5-2.3-3.6-5.8-5.4-10.6-5.4-4.4 0-8.1 1.7-11.2 5zM570.2 32.4c4.4-2.9 8.7-4.3 13-4.3 4 0 7.2 1.3 9.3 3.8 2.2 2.5 3.3 6.6 3.3 12.4 0 3.3-.8 9-2.4 17l-.6 2.9c-.1.6-.3 1.6-.6 2.9-1.3 7.9-2.1 13.2-2.4 16.1-.2 2.5.3 3.8 1.3 3.8 1.5 0 3.3-1 5.3-2.9l1.5 2.6c-.7 1.1-2.2 2.5-4.4 4-2.2 1.6-4.5 2.4-7.1 2.4-4 0-6-2.1-6-6.3 0-1.6.2-3.9.7-6.6.4-2.8.9-5.5 1.5-8.2l1-5.1.7-3.5c.8-4.6 1.4-8.2 1.8-10.8.4-2.6.6-5.2.6-7.6 0-3.7-.6-6.2-1.7-7.6-1.1-1.4-3.1-2.1-5.8-2.1-3.2 0-6.9 1.6-11 4.7s-7.5 6.8-10.4 11.2L552.3 92h-8.8l7.3-54.8-5.4-2.6.8-4 11.8-2.4 2.1 1.1-1.5 14.2c3.4-4.6 7.2-8.2 11.6-11.1z"/></g></svg>
            </a>
        </div>
        
		<nav>
            <ul class="main-menu">
                <li><a href="#" class="dropdown-trigger">Productos</a></li>
                <li><a href="page-advantages.php">Tus ventajas</a></li>
                <li><a href="page-blog.php">Ideas en cartón</a></li>
                <li><a href="page-members.php">Stocks</a></li>
            </ul>
		</nav>
	</header>
    
    <nav class="sidebar">
        <div class="sidebar-bg"></div>
        <div class="toggle-btn" id="toggle-btn">
            <span></span>
        </div>

        <ul>
            <li><a href="page-help.php" title="¿Te ayudamos?"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-help"></use></svg><span>¿Te ayudamos?</span></a></li>
            <li><a href="page-account.php" title="Tu cuenta"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-account"></use></svg><span>Tu cuenta</span></a></li>
            <li><a href="page-contact.php" title="Contacto"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-contact"></use></svg><span>Contacto</span></a></li>
            <li><a href="page-sendemail.php" title="Mándanos un email"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-email"></use></svg><span>Mándanos un email</span></a></li>
            <li><a href="tel:0034936730525" title="Teléfono: 93 673 05 25"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-phone"></use></svg><span>+34 93 673 05 25</span></a></li>
        </ul>
    </nav>
    
    
    <?php include("content-dropdown.php"); ?>
    
    
    <div class="filter">
        <form>
            <div class="select-wrapper">
                <select class="form-control form-select" id="edit-field-familia-target-id-selective" name="field_familia_target_id_selective">
                    <option value="All" selected="selected">Productos</option>
                    <option value="2486">Fibras recicladas</option>
                    <option value="1091">Fibras vírgenes</option>
                    <option value="2567">Cartón grueso</option>
                    <option value="1101">Cartón ondulado</option>
                    <option value="1109">Cartulinas recicladas de colores</option>
                    <option value="1111">Cartón plum</option>
                    <option value="1099">Papeles alimentación</option>
                    <option value="1059">Papeles regalo, decoración y diseño</option>
                    <option value="1075">Material de embalaje</option>
                    <option value="2463">Fundas elásticas de cartón</option>
                    <option value="1107">Ondas de cartón</option>
                </select>
            </div>

            <div class="select-wrapper">
                <select class="form-control form-select" id="edit-field-aplicacion-tid-selective" name="field_aplicacion_tid_selective">
                    <option value="All" selected="selected">Ideas</option>
                    <option value="5">Artículos de escritorio</option>
                    <option value="70">Comunicación corporativa</option>
                    <option value="2">Encuadernación de tapa dura</option>
                    <option value="6">Encuadernación tapa rústica</option>
                    <option value="71">Impresión y escritura</option>
                    <option value="68">Packaging</option>
                    <option value="69">Packaging flexible</option>
                </select>
            </div>

            <div class="select-wrapper">
                <select class="form-control form-select" id="edit-field-peso-value-selective" name="field_peso_value_selective">
                    <option value="All" selected="selected">Gramaje</option><option value="90.00">90 gr.</option><option value="100.00">100 gr.</option><option value="110.00">110 gr.</option><option value="115.00">115 gr.</option><option value="120.00">120 gr.</option><option value="125.00">125 gr.</option><option value="130.00">130 gr.</option><option value="135.00">135 gr.</option><option value="140.00">140 gr.</option><option value="150.00">150 gr.</option><option value="160.00">160 gr.</option><option value="165.00">165 gr.</option><option value="173.00">173 gr.</option><option value="175.00">175 gr.</option><option value="180.00">180 gr.</option><option value="190.00">190 gr.</option><option value="195.00">195 gr.</option><option value="200.00">200 gr.</option><option value="204.00">204 gr.</option><option value="218.00">218 gr.</option><option value="220.00">220 gr.</option><option value="225.00">225 gr.</option><option value="230.00">230 gr.</option><option value="233.00">233 gr.</option><option value="240.00">240 gr.</option><option value="241.00">241 gr.</option><option value="246.00">246 gr.</option><option value="250.00">250 gr.</option><option value="260.00">260 gr.</option><option value="270.00">270 gr.</option><option value="275.00">275 gr.</option><option value="300.00">300 gr.</option><option value="308.00">308 gr.</option><option value="330.00">330 gr.</option><option value="340.00">340 gr.</option><option value="350.00">350 gr.</option><option value="360.00">360 gr.</option><option value="380.00">380 gr.</option><option value="455.00">455 gr.</option><option value="470.00">470 gr.</option><option value="675.00">675 gr.</option><option value="2320.00">2320 gr.</option>
                </select>
            </div>

            <button type="submit" id="edit-reset" name="op" value="Reset filters" class="btn btn-default form-submit">Buscar</button>
        </form>
    </div>