<?php
$bodyClass = 'categories';
include 'header.php';
?>


<section class="billboard">
    
</section>


<main>
    
    <section class="categories-section container">
        
        <div class="row">
            <div class="col col-3"></div>
            
            <div class="col col-9">
                <div class="header-wrapper">
                    <h1><a href="page-catagories.php">Productos</a></h1> 
                    <nav>
                        <ul id="breadcrumbs" class="page-breadcrumbs">
                            <li>Fibras recicladas</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col col-3">
                
                <nav>
                    <ul class="product-categories">
                        <li class="current-cat"><a href="#">Fibras recicladas</a></li>
                        <li><a href="#">Fibras vírgenes</a></li>
                        <li><a href="#">Cartón grueso</a></li>
                        <li><a href="#">Cartón ondulado</a></li>
                        <li><a href="#">Cartulinas recicladas de colores</a></li>
                        <li><a href="#">Cartón pluma</a></li>
                        <li><a href="#">Papeles alimentación</a></li>
                        <li><a href="#">Papeles regalo, decoración y diseño</a></li>
                        <li><a href="#">Material de embalaje</a></li>
                        <li><a href="#">Fundas elásticas de cartón</a></li>
                        <li><a href="#">Ondas de cartón</a></li>
                    </ul>
                </nav>
                
            </div>

            <div class="col col-9">
                <div class="categories_list">
                    <ul class="cat-list">
                        <li><a href="page-products.php"><img width="240" height="160" src="assets/images/products/category-2.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="">Estucados</a></li>
                        <li><a href="page-products.php"><img width="240" height="160" src="assets/images/products/category-1.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="">No Estucados</a></li>
                    </ul>
                </div>
            </div>
        </div>
        
    </section>
    
    <?php include("content-aside.php"); ?>
    
</main>


<?php include("footer.php"); ?>