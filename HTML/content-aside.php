<aside>
    <div class="row cards">
        <div class="col">
            <div class="card-body">
                <svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-samples"></use></svg>
                <h4>¿Te enviamos una caja de muestras?</h4>
                <img src="assets/images/muestrario.png" width="128" height="100">
            </div>
            <div class="card-footer">
                <a href="#" class="button small" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
            </div>
        </div>

        <div class="col">
            <div class="card-body">
                <svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-idea"></use></svg>
                <h4>Ideas en cartón</h4><img src="assets/images/ideas.png" width="121" height="150">
            </div>
            <div class="card-footer">
                <a href="page-blog.php" class="button small" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
            </div>
        </div>

        <div class="col">
            <div class="card-body">
                <svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-percent"></use></svg>
                <h4>Stocks</h4>
                <p>Ven a ver las ofertas y liquidaciones de papel y cartón. Ti te suscribes al newsletter te informaremos antes que nadie.</p>
            </div>
            <div class="card-footer">
                <a href="page-members.php" class="button small" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
            </div>
        </div>
    </div>

    <div class="tellus">
        <div class="tellus-wrapper">

            <svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-info"></use></svg>

            <span>Conectamos contigo</span>

            <h3>Dinos qué haces y te diremos qué cartón necesitas</h3>

            <a href="page-help.php" class="button red float-right" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>

        </div>
    </div>
</aside>