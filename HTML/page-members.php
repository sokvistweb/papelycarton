<?php
$bodyClass = 'members';
include 'header.php';
?>


<section class="billboard">
    
</section>


<main>
    
    <section class="members-section container">
        
        <div class="row">
            <div class="col">
                <h1>Stocks</h1>
            </div>
        </div>
        
        <div class="row members-content">
            <div class="col col-4">
                
                <h2>Área Cliente</h2>
                <p>Lo sentimos, el contenido de esta página está restringido solo para miembros.</p>
                
            </div>
            
            <div class="col col-8">

                <form name="loginform" id="loginform" action="https://www.npld.eu/wp-login.php" method="post">
                    <p class="login-username">
                        <label for="user_login">Usuario</label>
                        <input type="text" name="log" id="user_login" class="input" value="" size="20">
                    </p>
                    <p class="login-password">
                        <label for="user_pass">Contraseña</label>
                        <input type="password" name="pwd" id="user_pass" class="input" value="" size="20">
                    </p>
                    <a class="lost-password" href="/wp-login.php?action=lostpassword">¿Perdiste la contraseña?</a>
                    <p class="login-submit">
                        <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary" value="Log In">
                        <input type="hidden" name="redirect_to" value="https://www.npld.eu/npld-documents/">
                    </p>
                </form>
                
            </div>
        </div>
        
    </section>
    
    <?php include("content-aside.php"); ?>
    
</main>


<?php include("footer.php"); ?>