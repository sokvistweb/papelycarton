
	<footer class="">
        <div class="footer-wrapper">
            <div class="row">
                <div class="col-3">
                    <h3 class="logo-footer">
                        <a href="#" rel="home"><svg class="icon-logo-footer"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-papelycarton-footer-color"></use></svg></a>
                    </h3>
                    <div class="footer-menu">
                        <ul>
                            <li><a href="page-catagories.php">Productos</a></li>
                            <li><a href="page-advantages.php">Tus ventajas</a></li>
                            <li><a href="page-blog.php">Ideas en cartón</a></li>
                            <li><a href="page-members.php">Stocks</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-3">
                    <div class="contact">
                        <h3><a href="https://goo.gl/maps/zPGovaxxmHacXCai8" title="Ver en Google Maps" target="_blank"><svg><use xlink:href="assets/images/icons/symbol-defs.svg#icon-location"></use></svg>Contacto</a></h3>
                        <address>C/ Garnatxa,12<br>
                            Parc Empresarial de Cervelló<br>
                            08758 Cervelló (Barcelona)</address>

                        <p>Teléfono: <a href="tel:0034936730525">+34 93 673 05 25</a><br>
                            Fax: <a href="tel:0034936730525">+34 93 673 05 26</a><br>
                        <a href="mailto:vegio@vegio.es">vegio@vegio.es</a>
                        </p>
                    </div>
                </div>

                <div class="col-2">
                </div>

                <div class="col-4">
                    <div class="subscribe">
                        <h3 class="title">Te informamos de nuestras novedades</h3>
                        <p>Suscríbete al newsletter</p>
                        <!-- Begin MailChimp Signup Form -->
                        <form action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <div id="mc_embed_signup_scroll">
                                <input type="email" value="" placeholder="Email" name="EMAIL" class="required email" id="mce-EMAIL">
                                <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn">

                                <div id="mce-responses" class="clear">
                                    <div class="response" id="mce-error-response" style="display:none"></div>
                                    <div class="response" id="mce-success-response" style="display:none"></div>
                                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d3c9b15aaed48a8fe8fdbbf47_8b2bfb5fe4" tabindex="-1" value=""></div>
                            </div>
                        </form>
                        <!--End mc_embed_signup-->
                    </div>
                </div>
            </div>
        </div>
        
		<div class="subfooter">
            <p><a href="https://www.vegio.es/" target="blank">&copy; 2020 Vegio, S.L.</a> · <a href="page-legal.php" target="_blank">Política de privacidad</a> · <a href="/terminos-y-condiciones">Términos y condiciones</a> · <a href="/politica-de-cookies">Política de cookies</a></p>
        </div>
	</footer>


    <script type="text/javascript" src="assets/js/vendor/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="assets/js/min/plugins.min.js"></script>
    <script type="text/javascript" src="assets/js/min/main.min.js"></script>


</body>
</html>
