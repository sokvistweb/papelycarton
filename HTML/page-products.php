<?php
$bodyClass = 'categories';
include 'header.php';
?>


<section class="billboard">
    
</section>


<main>
    
    <section class="categories-section products-section container">
        
        <div class="row">
            <div class="col col-3"></div>
            
            <div class="col col-9">
                <div class="header-wrapper">
                    <h1><a href="page-catagories.php">Productos</a></h1> 
                    <nav>
                        <ul id="breadcrumbs" class="page-breadcrumbs"> 
                            <li><a href="page-catagories.php">Fibras Recicladas</a></li> &sol; 
                            <li>No Estucados</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col col-3">
                
                <nav>
                    <ul class="product-categories">
                        <li class="current-cat"><a href="page-catagories.php">Fibras recicladas</a></li>
                        <li><a href="#">Fibras vírgenes</a></li>
                        <li><a href="#">Cartón grueso</a></li>
                        <li><a href="#">Cartón ondulado</a></li>
                        <li><a href="#">Cartulinas recicladas de colores</a></li>
                        <li><a href="#">Cartón pluma</a></li>
                        <li><a href="#">Papeles alimentación</a></li>
                        <li><a href="#">Papeles regalo, decoración y diseño</a></li>
                        <li><a href="#">Material de embalaje</a></li>
                        <li><a href="#">Fundas elásticas de cartón</a></li>
                        <li><a href="#">Ondas de cartón</a></li>
                    </ul>
                </nav>
                
            </div>

            <div class="col col-9">
                <div class="products_list">
                    <ul class="product-list">
                        <li class="product-card">
                            <a href="page-single-product.php"><img width="300" height="200" src="assets/images/products/category-2.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></a>
                            <h2><a href="page-single-product.php">Cartón Arena</a></h2>
                            <div class="btn-wrapper">
                                <a href="page-single-product.php" class="button small" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
                            </div>
                        </li>
                        <li class="product-card">
                            <a href="page-single-product.php"><img width="300" height="200" src="assets/images/products/category-1.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></a>
                            <h2><a href="page-single-product.php">Cartón Satinado</a></h2>
                            <div class="btn-wrapper">
                                <a href="page-single-product.php" class="button small" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
                            </div>
                        </li>
                        <li class="product-card">
                            <a href="page-single-product.php"><img width="300" height="200" src="assets/images/products/category-3.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></a>
                            <h2><a href="page-single-product.php">Cartón Bigris</a></h2>
                            <div class="btn-wrapper">
                                <a href="page-single-product.php" class="button small" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
                            </div>
                        </li>
                        <li class="product-card">
                            <a href="page-single-product.php"><img width="300" height="200" src="assets/images/products/category-4.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></a>
                            <h2><a href="page-single-product.php">Cartón Skin Pack 1 Cara</a></h2>
                            <div class="btn-wrapper">
                                <a href="page-single-product.php" class="button small" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
                            </div>
                        </li>
                        <li class="product-card">
                            <a href="page-single-product.php"><img width="300" height="200" src="assets/images/products/category-5.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></a>
                            <h2><a href="page-single-product.php">Cartón Skin Pack 2 Caras</a></h2>
                            <div class="btn-wrapper">
                                <a href="page-single-product.php" class="button small" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
                            </div>
                        </li>
                        <li class="product-card">
                            <a href="page-single-product.php"><img width="300" height="200" src="assets/images/products/category-6.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></a>
                            <h2><a href="page-single-product.php">Cartón Teja</a></h2>
                            <div class="btn-wrapper">
                                <a href="page-single-product.php" class="button small" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
                            </div>
                        </li>
                        <li class="product-card">
                            <a href="page-single-product.php"><img width="300" height="200" src="assets/images/products/category-7.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset=""></a>
                            <h2><a href="page-single-product.php">Cartón Azulete</a></h2>
                            <div class="btn-wrapper">
                                <a href="page-single-product.php" class="button small" title="Saber más"><svg class="icon"><use xlink:href="assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        
    </section>
    
    <?php include("content-aside.php"); ?>
    
</main>


<?php include("footer.php"); ?>