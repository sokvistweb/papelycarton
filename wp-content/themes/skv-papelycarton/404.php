<?php get_header(); ?>

<section class="advantages-section container">
    
    <div class="row">
        <div class="col">

        <h1><?php _e( 'Page not found', 'html5blank' ); ?></h1>
            
        </div>
    </div>
    
    <div class="row">
        <div class="col">
            
            <h2>
				<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'html5blank' ); ?></a>
            </h2>
            
            <div class="byebyecube-wrapper">
                <div id="byebyecube"></div>
            </div>
            
        </div>
    </div>

</section>

<?php get_footer(); ?>
