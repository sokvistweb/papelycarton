<?php get_header(); ?>

<section class="blog-section container">
    
    <div class="row">
        <div class="col">
            <h1 class="category-for"><?php echo sprintf( __( '<span>%s resultados para:</span> ', 'html5blank' ), $wp_query->found_posts ); echo get_search_query(); ?></h1>
        </div>
    </div>

    <div class="row">
        <div class="col col-12">
            <div class="posts_list">
                <ul class="post-list">
                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                    <li class="post-card" id="post-<?php the_ID(); ?>">
                        <!-- post thumbnail -->
                        <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                            <?php the_post_thumbnail(array(900,600)); // Declare pixel size you need inside the array ?>
                        <?php endif; ?>
                        <!-- /post thumbnail -->
                            
                        <!-- post title -->
                        <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                        <!-- /post title -->

                        <!--div class="entry-meta">
                            <span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
                        </div>-->
                        <div class="entry-content clearfix">
                            <?php html5wp_excerpt('html5wp_index'); // Dynamic Content ?>
                        </div>

                        <div class="btn-wrapper">
                            <a href="<?php the_permalink(); ?>" class="button small" title="<?php the_title(); ?>"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
                        </div>
                    </li>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
            </div>

            <nav class="pagination">
                <?php wpex_pagination(); ?>
            </nav>
        </div>

        <!--<div class="col col-3">
            <div class="widget widget_categories">
                <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-area-2')) ?>
            </div>
        </div>-->
    </div>
    
</section>


<?php get_footer(); ?>

