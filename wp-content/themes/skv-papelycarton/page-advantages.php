<?php /* Template Name: Tus Ventajas */ get_header(); ?>


<section class="advantages-section container">
    
    <div class="row">
        <div class="col">

        <h1><?php the_title(); ?></h1>
            
        </div>
    </div>
    
    <div class="row">
        <div class="col">

    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

        <nav>
            <ul class="advantages-list">
                <?php if( get_field('ventaja_1') ): ?>
                <li><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-checkmark"></use></svg>
                    <?php the_field('ventaja_1'); ?>
                </li>
                <?php endif; ?>
                <?php if( get_field('ventaja_2') ): ?>
                <li><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-checkmark"></use></svg>
                    <?php the_field('ventaja_2'); ?>
                </li>
                <?php endif; ?>
                <?php if( get_field('ventaja_3') ): ?>
                <li><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-checkmark"></use></svg>
                    <?php the_field('ventaja_3'); ?>
                </li>
                <?php endif; ?>
                <?php if( get_field('ventaja_4') ): ?>
                <li><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-checkmark"></use></svg>
                    <?php the_field('ventaja_4'); ?>
                </li>
                <?php endif; ?>
                <?php if( get_field('ventaja_5') ): ?>
                <li><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-checkmark"></use></svg>
                    <?php the_field('ventaja_5'); ?>
                </li>
                <?php endif; ?>
                <?php if( get_field('ventaja_6') ): ?>
                <li><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-checkmark"></use></svg>
                    <?php the_field('ventaja_6'); ?>
                </li>
                <?php endif; ?>
                <?php if( get_field('ventaja_7') ): ?>
                <li><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-checkmark"></use></svg>
                    <?php the_field('ventaja_7'); ?>
                </li>
                <?php endif; ?>
                <?php if( get_field('ventaja_8') ): ?>
                <li><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-checkmark"></use></svg>
                    <?php the_field('ventaja_8'); ?>
                </li>
                <?php endif; ?>
                <?php if( get_field('ventaja_9') ): ?>
                <li><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-checkmark"></use></svg>
                    <?php the_field('ventaja_9'); ?>
                </li>
                <?php endif; ?>
            </ul>
        </nav>

    <?php endwhile; ?>
            
        </div>
    </div>
    
    <div class="row">
        <div class="col">

    <?php else: ?>

        <!-- article -->
        <article>

            <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

        </article>
        <!-- /article -->

    <?php endif; ?>
            
        </div>
    </div>

</section>


<?php get_footer(); ?>
