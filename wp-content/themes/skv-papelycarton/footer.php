
<?php if ( is_woocommerce() ) : ?>
    
    <?php get_template_part( 'templates/content', 'aside_pages' ); ?>
    
<?php else: ?>
    
    <?php get_template_part( 'templates/content', 'aside' ); ?>
    
<?php endif; ?>


</main>


        <!-- Cookies popup -->
    <div class="eupopup-container eupopup-container-bottomright"> 
        <div class="eupopup-markup">
            <div class="eupopup-head"></div> 
            <div class="eupopup-body">Esta web utiliza cookies propias para obtener información estadística. Al clicar en "Acepto" aceptas su uso.</div> 
            <div class="eupopup-buttons"> 
                <a href="#" class="eupopup-button eupopup-button_1">Acepto</a> 
                <a href="<?php bloginfo('home') ?>/politica-de-cookies/" target="_blank" class="eupopup-button eupopup-button_2">Más info</a> 
            </div> 
            <div class="clearfix"></div> 
            <a href="#" class="eupopup-closebutton">
                <svg><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-close"></use></svg>
            </a> 
        </div> 
    </div><!-- /Cookies popup -->


	<footer class="">
        <div class="footer-wrapper">
            <div class="row">
                <div class="col-3">
                    <h3 class="logo-footer">
                        <a href="#" rel="home"><svg class="icon-logo-footer"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-papelycarton-footer-color"></use></svg></a>
                    </h3>
                    <div class="footer-menu">
                        <?php footer_nav(); ?>
                    </div>
                </div>

                <div class="col-2">
                    <div class="contact">
                        <h3><a href="https://goo.gl/maps/zPGovaxxmHacXCai8" title="Ver en Google Maps" target="_blank"><svg><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-location"></use></svg>Contacto</a></h3>
                        <address>C/ Garnatxa,12<br>
                            Parc Empresarial de Cervelló<br>
                            08758 Cervelló (Barcelona)</address>

                        <p>Teléfono: <a href="tel:0034936730525">+34 93 673 05 25</a><br>
                        <a href="mailto:vegio@vegio.es">vegio@vegio.es</a>
                        </p>
                    </div>
                </div>

                <div class="col-3">
                    <a href="https://papelycarton.com/wp-content/uploads/2022/09/FSC-CERTIFICADO-VEGIO-2022-2024.pdf" class="logo-fsc" title="Forest Stewardship Counsil website" target="_blank"><svg class="icon-logo-fsc"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-fscorg-logo"></use></svg></a>
                </div>
                

                <div class="col-4">
                    <div class="subscribe">
                        <!-- 
                        <h3 class="title">Te informamos de nuestras novedades</h3>
                        <p>Suscríbete al newsletter</p>
                        Begin MailChimp Signup Form 
                        <form action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <div id="mc_embed_signup_scroll">
                                <input type="email" value="" placeholder="Email" name="EMAIL" class="required email" id="mce-EMAIL">
                                <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn">

                                <div id="mce-responses" class="clear">
                                    <div class="response" id="mce-error-response" style="display:none"></div>
                                    <div class="response" id="mce-success-response" style="display:none"></div>
                                </div>
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d3c9b15aaed48a8fe8fdbbf47_8b2bfb5fe4" tabindex="-1" value=""></div>
                            </div>
                        </form>
                        End mc_embed_signup-->
                    </div>
                </div>
            </div>
        </div>
        
		<div class="subfooter">
            <p>Vegio, S.L. · <a href="/aviso-legal/" title="Aviso legal">Aviso legal</a> · <a href="/politica-privacidad/" title="Política de privacidad">Política de privacidad</a> · <a href="/politica-de-cookies/" title="Política de cookies">Política de cookies</a> · <a href="/condiciones-generales-de-venta/" title="Condiciones Generales de Venta">Condiciones Generales de Venta</a></p>
        </div>
	</footer>


    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/vendor/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/min/plugins.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/min/main.min.js"></script>

    <script type="text/javascript"> _linkedin_partner_id = "3658609"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=3658609&fmt=gif" /> </noscript>


</body>
</html>

