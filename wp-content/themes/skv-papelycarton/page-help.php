<?php /* Template Name: Te ayudamos */ get_header(); ?>


<section class="help-section container">
    
    <div class="row">
        <div class="col">

        <h1><?php the_title(); ?></h1>
            
        </div>
    </div>
    
    <div class="row help-content">
        <div class="col col-4">
            <?php the_content(); ?>
        </div>

        <div class="col col-8">
            
            <?php echo do_shortcode( '[contact-form-7 id="152" title="Formulario Te Ayudamos"]' ); ?>
            
        </div>
    </div>

</section>


<?php get_footer(); ?>
