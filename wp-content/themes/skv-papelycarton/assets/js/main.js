/* =============================================================================
   jQuery: all the custom stuff! 
   ========================================================================== */

jQuery(document).ready(function($){
    
    
    /*  Animated header */
    var changeHeader = 160;
    $(window).scroll(function() {
        var scroll = getCurrentScroll();
        if ( scroll >= changeHeader ) {
            $('body').addClass('scrolled');
            }
            else {
                $('body').removeClass('scrolled');
            }
    });
    
    function getCurrentScroll() {
        return window.pageYOffset;
    }
    
    
    /* open/close products panel *****/
	$('.dropdown-trigger a').on('click', function(event){
		event.preventDefault();
		toggleNav();
	});

	// close panel
	$('.products-dropdown .cd-close').on('click', function(event){
		event.preventDefault();
		toggleNav();
	});

	function toggleNav(){
		var navIsVisible = ( !$('.products-dropdown').hasClass('dropdown-is-active') ) ? true : false;
		$('.products-dropdown').toggleClass('dropdown-is-active', navIsVisible);
		$('.dropdown-trigger a').toggleClass('dropdown-is-active', navIsVisible);
		
	}
    
    
    /*  Fixed side bar open/close *****/
    var open = false;

    var openSidebar = function(){
        $('.sidebar').addClass("opened");
        $('.toggle-btn').addClass('toggle');
        open = true;
    }
    var closeSidebar = function(){
        $('.sidebar').removeClass("opened");
        $('.toggle-btn').removeClass('toggle');
        open = false;
    }

    $('.toggle-btn').click( function(event) {
        event.stopPropagation();
        var toggle = open ? closeSidebar : openSidebar;
        toggle();
    });

    $(document).click( function(event){
        if ( !$(event.target).closest('.sidebar').length ) {
            closeSidebar();   
        }
    });
    
    
    
    /*  Show/hide main menu
        https://codepen.io/ocidique/pen/tlGEL */
    $('#show-hidden-menu').click(function() {
        $('.main-menu li:not(:first-child)').slideToggle("fast");
    });
    
    
    
    /* mgAccordion https://github.com/marqusG/mgAccordion *****/ 
    $('.products-list').mgaccordion({
        //theme: 'tree',
        leaveOpen: false
    });
    
    
    
    // Count and show child elements
    /*$('.dropdown').each(function(){
        var numChilds = $('li a', $(this)).length;
        $('.toggler', $(this)).html(numChilds);
    }); ** adding count via php */
    
    
    
    // Show/hide fields on help form
    // https://www.tutorialrepublic.com/faq/show-hide-divs-based-on-radio-button-selection-in-jquery.php
    $('input[name=radio-client]').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        //$(".box").not(targetBox).hide();
        //$(targetBox).show();
        $(".box-fields").not(targetBox).removeClass('togglefield');
        $(targetBox).addClass('togglefield');
    });
    
    // Change span text (different to radio value)
    $('.radio-client .first label .wpcf7-list-item-label').text('No soy cliente');
    $('.radio-client .last label .wpcf7-list-item-label').text('Soy cliente');
    
    
    
    
    // Add .select-wrapper class to select
    $('.searchandfilter ul li:not(:last-child)').addClass('select-wrapper');
    
    
});

