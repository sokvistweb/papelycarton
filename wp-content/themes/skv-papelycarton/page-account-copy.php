<?php /* Template Name: Tu cuenta */ get_header(); ?>


<section class="members-section container">
    
    <div class="row">
        <div class="col">

        <h1><?php the_title(); ?></h1>
            
        </div>
    </div>
    
    <div class="row  members-content">
        <div class="col col-4">
            
            <h2><?php esc_html_e( 'Login', 'woocommerce' ); ?></h2>
            
        </div>

        <div class="col col-8">
            
            <?php the_content(); ?>
            
        </div>
    </div>

</section>


<?php get_footer(); ?>
