<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<section id="product-<?php the_ID(); ?>" <?php wc_product_class( 'single-product-section container', $product ); ?>>
    
    <div class="row">
        <div class="col image">
        <?php
        /**
         * Hook: woocommerce_before_single_product_summary.
         *
         * @hooked woocommerce_show_product_sale_flash - 10
         * @hooked woocommerce_show_product_images - 20
         */
        do_action( 'woocommerce_before_single_product_summary' );
        ?>
        </div>

        <div class="col content">
            <div class="product_header entry-header">
                <div class="row">
                    <div class="col col-10">
                        <span class="col-title">Producto</span>
                        <nav>
                            <?php woocommerce_breadcrumb(); ?>
                        </nav>
                        <?php the_title( '<h1 class="product_title entry-title">', '</h1>' ); ?>
                    </div>
                
                    <div class="col col-2 product_meta">
                        <span class="col-title">Referencia</span>
                        <?php do_action( 'woocommerce_product_meta_start' ); ?>

                        <?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

                            <span class="sku_wrapper"><?php esc_html_e( '', 'woocommerce' ); ?> <span class="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></span></span>

                        <?php endif; ?>

                        <?php do_action( 'woocommerce_product_meta_end' ); ?>

                    </div>
                </div><!-- /row -->
            </div><!-- /product_header -->
            
            <div class="product_content entry-content">
                <div class="row">
                    <div class="col">
                        <span class="col-title">Descripción</span>
                        
                        <?php the_content(); ?>

                        <?php if ( 'yes' == get_field('apto_alimentos') ): ?>
                            <p class="suitable"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-checkmark"></use></svg><?php the_field('texto_apto'); ?></p>
                        <?php else: ?>
                            <!-- nothing -->
                        <?php endif; ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span class="col-title">Aplicaciones</span>
                        
                        <?php the_excerpt(); ?>
                        
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span class="col-title">Calibres (mm)</span>
                        <?php
                            // https://stackoverflow.com/questions/49630032/display-specific-product-attributes-on-woocommerce-archives-pages
                        global $product;
                        $calibres_val = $product->get_attribute('calibres');
                            //echo $calibres_val;
                            //echo '<p>' . print_r( $calibres_val, TRUE ) . '</p>';
                            echo '<p>' . str_replace( ',', '', $calibres_val ) . '</p>';
                        ?>
                        
                        
                    </div>
                    <div class="col">
                        <span class="col-title">Medidas</span>
                        <?php if( get_field('medidas') ): ?>
                        <p><?php the_field('medidas'); ?></p>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <span class="col-title">Gramajes (gr/m2)</span>
                        <?php
                        global $product;
                        $gramajes_val = $product->get_attribute('gramajes');
                            //echo $gramajes_val;
                            //echo '<p>' . print_r( $gramajes_val, TRUE ) . '</p>';
                            echo '<p>' . str_replace( ',', '', $gramajes_val ) . '</p>';
                        ?>
                          
                    </div>
                    <div class="col">
                        <span class="col-title">Observaciones</span>
                        <?php if( get_field('observaciones') ): ?>
                        <?php the_field('observaciones'); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div><!-- /product_content -->

            
            <div class="product_downloads">
                <div class="row">
                    <div class="col">
                        <span class="col-title">Descargas</span>
                        <div class="downloands-wrapper">
                            <?php if( have_rows('descargas') ): ?>
                            <?php while( have_rows('descargas') ): the_row(); ?>
                            
                            <?php if( get_sub_field('link_archivo_1') && get_sub_field('nombre_archivo_1') ): ?>
                            <a href="<?php the_sub_field('link_archivo_1'); ?>" class="button download" title="Descargar ficha técnica"><?php the_sub_field('nombre_archivo_1'); ?><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-file-download-alt"></use></svg></a>
                            <?php endif; ?>
                            
                            <?php if( get_sub_field('link_archivo_2') && get_sub_field('nombre_archivo_2') ): ?>
                            <a href="<?php the_sub_field('link_archivo_2'); ?>" class="button download" title="Descargar ficha técnica"><?php the_sub_field('nombre_archivo_2'); ?><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-file-download-alt"></use></svg></a>
                            <?php endif; ?>
                            
                            <?php if( get_sub_field('link_archivo_3') && get_sub_field('nombre_archivo_3') ): ?>
                            <a href="<?php the_sub_field('link_archivo_3'); ?>" class="button download" title="Descargar ficha técnica"><?php the_sub_field('nombre_archivo_3'); ?><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-file-download-alt"></use></svg></a>
                            <?php endif; ?>
                            
                            <?php if( get_sub_field('link_archivo_4') && get_sub_field('nombre_archivo_4') ): ?>
                            <a href="<?php the_sub_field('link_archivo_4'); ?>" class="button download" title="Descargar ficha técnica"><?php the_sub_field('nombre_archivo_4'); ?><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-file-download-alt"></use></svg></a>
                            <?php endif; ?>
                            
                            <?php if( get_sub_field('link_archivo_5') && get_sub_field('nombre_archivo_5') ): ?>
                            <a href="<?php the_sub_field('link_archivo_5'); ?>" class="button download" title="Descargar ficha técnica"><?php the_sub_field('nombre_archivo_5'); ?><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-file-download-alt"></use></svg></a>
                            <?php endif; ?>
                            
                            <?php if( get_sub_field('link_archivo_6') && get_sub_field('nombre_archivo_6') ): ?>
                            <a href="<?php the_sub_field('link_archivo_6'); ?>" class="button download" title="Descargar ficha técnica"><?php the_sub_field('nombre_archivo_6'); ?><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-file-download-alt"></use></svg></a>
                            <?php endif; ?>
                            
                            <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
                
            <!-- *** Price and Add to cart button, to be used whan shop is enabled *** -->
            <p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) ); ?>"><?php echo $product->get_price_html(); ?></p>

            <div class="add-to-cart">
                <?php if ( $product->is_in_stock() ) : ?>

                <?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

                <form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
                    <?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

                    <?php
                    do_action( 'woocommerce_before_add_to_cart_quantity' );

                    woocommerce_quantity_input(
                        array(
                            'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
                            'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
                            'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
                        )
                    );

                    do_action( 'woocommerce_after_add_to_cart_quantity' );
                    ?>

                    <button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

                    <?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
                </form>

                <?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

            <?php endif; ?>  
            </div>
            <!-- *** Price and Add to cart button, to be used whan shop is enabled *** -->
			
			<div class="summary entry-summary">
		<?php
		/**
		 * Hook: woocommerce_single_product_summary.
		 *
		 * @hooked woocommerce_template_single_title - 5
		 * @hooked woocommerce_template_single_rating - 10
		 * @hooked woocommerce_template_single_price - 10
		 * @hooked woocommerce_template_single_excerpt - 20
		 * @hooked woocommerce_template_single_add_to_cart - 30
		 * @hooked woocommerce_template_single_meta - 40
		 * @hooked woocommerce_template_single_sharing - 50
		 * @hooked WC_Structured_Data::generate_product_data() - 60
		 */
		do_action( 'woocommerce_single_product_summary' );
		?>
	</div>
            
        </div>
    </div>
        
</section>


<section class="wrapper">

    <div class="product-inquiry">
        <div class="row">
            <div class="col">
                <?php echo do_shortcode( '[contact-form-7 id="705" title="Formulario ficha del producto"]' ); ?>
            </div>
        </div>
    </div>

</section>


<?php get_template_part( 'templates/content', 'related_products' ); ?>


<?php do_action( 'woocommerce_after_single_product' ); ?>
