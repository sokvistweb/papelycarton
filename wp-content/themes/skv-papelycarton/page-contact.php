<?php /* Template Name: Contacto */ get_header(); ?>


<section class="categories-section container">
    
    <div class="row">
        <div class="col">

        <h1><?php the_title(); ?></h1>
            
        </div>
    </div>
    
    <div class="row help-content contact-content">
        <div class="col col-4">
            <?php the_content(); ?>
        </div>

        <div class="col col-8">
            <?php echo do_shortcode( '[contact-form-7 id="155" title="Formulario de contacto"]' ); ?>
        </div>
    </div>

</section>


<?php get_footer(); ?>
