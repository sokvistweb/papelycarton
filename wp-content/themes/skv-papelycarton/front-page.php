<?php get_header(); ?>


<section class="features" id="features">
    <?php if( have_rows('parte_superior') ): ?>
    <?php while( have_rows('parte_superior') ): the_row(); ?>
    <span class="qualities"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-albums"></use></svg><?php the_sub_field('titulo'); ?></span>

    <div class="row">
        <div class="col">
            <div class="features-content">
                <h3><?php the_sub_field('top_left_1'); ?></h3>
                <h3><?php the_sub_field('top_left_2'); ?></h3>
            </div>
        </div>

        <div class="col">
            <div class="features-content">
                <h3><?php the_sub_field('top_right'); ?></h3>
                <svg class=""><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-recycle"></use></svg>
            </div>
        </div>
    </div>
    <?php endwhile; ?>
    <?php endif; ?>
</section>

<section class="efficiency wrapper">
    <?php if( have_rows('contenido_principal') ): ?>
    <?php while( have_rows('contenido_principal') ): the_row(); ?>
    <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-info"></use></svg>

    <h2><?php the_sub_field('frase_1'); ?></h2>

    <a href="<?php the_sub_field('link_frase_1'); ?>" class="button red float-right" title="Saber más"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
    <?php endwhile; ?>
    <?php endif; ?>
</section>


<?php get_footer(); ?>
