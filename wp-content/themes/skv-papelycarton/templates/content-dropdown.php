<?php 
    /* 
       Displays products dropdown panel */
?>

<div class="products-dropdown">
    <a href="#0" class="cd-close">Close</a>
    
    <nav class="products-menu">
        <ul class="products-list mg-accordion mg-flat">
            <!-- https://wordpress.stackexchange.com/questions/95491/category-count-inside-the-link -->
            <?php
                $categories = wp_list_categories( array(
                    'taxonomy' => 'product_cat',
                    'exclude'  => array( 15 ),
                    'show_count' => 1,
                    'title_li' => '',
                    'echo' => 0
                ) );
                $categories = preg_replace('/<\/a> \(([0-9]+)\)/', ' <span class="count">\\1</span></a>', $categories);
                echo $categories;
            ?>
        </ul>
    </nav>
    
</div>