<?php 
    /* Template Name: Related Products
       Displays related products */
?> 

<?php
global $product;

if( ! is_a( $product, 'WC_Product' ) ){
    $product = wc_get_product(get_the_id());
}

woocommerce_related_products( array(
    'posts_per_page' => 6,
    'columns'        => 6,
    'orderby'        => 'rand'
 ) );

?>
