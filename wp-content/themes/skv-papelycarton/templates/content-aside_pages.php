<aside class="aside-page">
    
    <div class="row">
        <div class="col">
        </div>
    </div>

    <div class="tellus">
        
        <div class="tellus-wrapper">
            <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-info"></use></svg>
            
            <?php if( have_rows('contenido_principal', 2) ): ?>
            <?php while( have_rows('contenido_principal', 2) ): the_row(); ?>
            <span><?php the_sub_field('subfrase_2'); ?></span>
            <h3><?php the_sub_field('frase_2'); ?></h3>
            
            <a href="<?php the_sub_field('link_frase_2'); ?>" class="button red float-right" title="Saber más"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
            <?php endwhile; ?>
            <?php endif; ?>

        </div>
        
    </div>
    
</aside>