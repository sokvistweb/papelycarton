<?php 
    /* 
       Displays products dropdown panel */
?>

<div class="products-dropdown">
    <a href="#0" class="cd-close">Close</a>
    
    <nav class="products-menu">
        
        <!--<?php
        if ( is_active_sidebar( 'widget-area-2' ) ) { ?>
            <ul class="products-list">
                <?php dynamic_sidebar( 'widget-area-2' ); ?>
            </ul>
        <?php }
        ?>-->
        
        <!--<ul class="products-list mg-accordion mg-flat">
            <?php wp_list_categories( array(
                'taxonomy' => 'product_cat',
                'exclude'  => array( 15 ),
                'show_count' => 1,
                'title_li' => ''
            ) ); ?>
        </ul>-->
        
        <ul class="products-list mg-accordion mg-flat">
            <!-- https://wordpress.stackexchange.com/questions/95491/category-count-inside-the-link -->
            <?php
                $categories = wp_list_categories( array(
                    'taxonomy' => 'product_cat',
                    'exclude'  => array( 15 ),
                    'show_count' => 1,
                    'title_li' => '',
                    'echo' => 0
                ) );
                $categories = preg_replace('/<\/a> \(([0-9]+)\)/', ' <span class="count">\\1</span></a>', $categories);
                echo $categories;
            ?>
        </ul>
        
        <!--<?php products_nav(); ?>-->
        
        
        <!--<?php

        // Setup your custom query
        $args = array( 'post_type' => 'product' );
        $loop = new WP_Query( $args );

        while ( $loop->have_posts() ) : $loop->the_post(); ?>

               <a href="<?php echo get_permalink( $loop->post->ID ) ?>">
                   <?php the_title(); ?>
               </a>

        <?php endwhile; wp_reset_query(); // Remember to reset ?>-->
        
        
        
        
        <!-- https://stackoverflow.com/questions/23561460/get-woocommerce-subcategory-products 
        
        <ul class="wsubcategs">
        <?php
        $wsubargs = array(
            'exclude'  => array( 15 ),
            'show_count' => 0,
            'title_li' => '',
            'parent' => $category->term_id,
            'taxonomy' => 'product_cat'
        );
        $wsubcats = get_categories($wsubargs);
        foreach ($wsubcats as $wsc):
        ?>
            <li>
                <a href="<?php echo get_term_link( $wsc->slug, $wsc->taxonomy );?>"><?php echo $wsc->name;?></a>
                <?php $subcategory_products = new WP_Query( array( 'post_type' => 'product', 'product_cat' => $wsc->slug ) );
                    if($subcategory_products->have_posts()):?>
                <ul class="subcat-products">
                    <?php while ( $subcategory_products->have_posts() ) : $subcategory_products->the_post(); ?>
                    <li>
                        <a href="<?php echo get_permalink( $subcategory_products->post->ID ) ?>">
                            <?php the_title(); ?>
                        </a>
                    </li>
                    <?php endwhile;?>
                </ul>
            <?php endif; wp_reset_query(); // Remember to reset ?>
            </li>
        <?php endforeach;?>
        </ul>-->
        
        
    </nav>
    
</div>