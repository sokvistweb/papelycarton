<aside>
        
    <div class="row cards">
        <div class="col">
            <?php if( have_rows('contenido_principal', 2) ): ?>
            <?php while( have_rows('contenido_principal', 2) ): the_row(); ?>
            <div class="card-body">
                <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-samples"></use></svg>
                <h4><?php the_sub_field('muestras'); ?></h4>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/muestrario.png" width="128" height="100">
            </div>
            <div class="card-footer">
                <a href="<?php the_sub_field('link_muestras'); ?>" class="button small" title="Saber más"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>

        <div class="col">
            <div class="card-body">
                <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-idea"></use></svg>
                <h4><?php $page = get_page_by_path( 'ideas-con-carton' ); echo get_the_title( $page );?></h4><img src="<?php echo get_template_directory_uri(); ?>/assets/images/ideas.png" width="121" height="150">
            </div>
            <div class="card-footer">
                <a href="<?php echo get_permalink( get_page_by_path( 'ideas-con-carton' ) ); ?>" class="button small" title="Saber más"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
            </div>
        </div>

        <div class="col">
            <?php wp_reset_query(); ?>
            <?php if (have_posts()) : ?>
            <?php query_posts(array( 'post_type' => 'page' , 'pagename' => 'stocks' )); ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="card-body">
                <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-percent"></use></svg>
                <h4><?php the_title(); ?></h4>
                <p><?php the_excerpt(); ?></p>
            </div>
            <div class="card-footer">
                <a href="<?php the_permalink(); ?>" class="button small" title="Saber más"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
        </div>
    </div>

    <div class="tellus">
        
        <div class="tellus-wrapper">
            <svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-info"></use></svg>
            
            <?php if( have_rows('contenido_principal', 2) ): ?>
            <?php while( have_rows('contenido_principal', 2) ): the_row(); ?>
            <span><?php the_sub_field('subfrase_2'); ?></span>
            <h3><?php the_sub_field('frase_2'); ?></h3>
            
            <a href="<?php the_sub_field('link_frase_2'); ?>" class="button red float-right" title="Saber más"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/symbol-defs.svg#icon-arrow-right"></use></svg></a>
            <?php endwhile; ?>
            <?php endif; ?>

        </div>
        
    </div>
    
</aside>