<?php get_header(); ?>


<main>
    
    <section class="blog-section container">
        
        <div class="row">
            <div class="col">
                <h1><?php the_title(); ?></h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col">
                <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <div class="row entry-content single-content">
                    <div class="col">
                        <!-- post thumbnail -->
                        <?php if ( has_post_thumbnail() ) : // Check if Thumbnail exists. ?>
                            <?php the_post_thumbnail(); // Fullsize image for the single post. ?>
                        <?php endif; ?>
                        <!-- /post thumbnail -->
                    </div>
                    
                    <div class="col">
                        <?php the_content(); // Dynamic Content. ?>
                    </div>
                </div>
                <?php endwhile; ?><?php endif; ?>
            </div>
        </div>
        
    </section>
    
</main>


<?php get_footer(); ?>
