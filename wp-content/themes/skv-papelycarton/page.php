<?php get_header(); ?>


<section class="advantages-section container">
    
    <div class="row">
        <div class="col">

        <h1><?php the_title(); ?></h1>
            
        </div>
    </div>
    
    <div class="row">
        <div class="col">

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

        <!-- article -->
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <?php the_content(); ?>

        </article>
        <!-- /article -->

    <?php endwhile; ?>
            
        </div>
    </div>
    
    <div class="row">
        <div class="col">

    <?php else: ?>

        <!-- article -->
        <article>

            <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

        </article>
        <!-- /article -->

    <?php endif; ?>
            
        </div>
    </div>

</section>


<?php get_footer(); ?>
